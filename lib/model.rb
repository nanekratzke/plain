require 'json'
require 'IPAddr'
require 'logger'
require 'set'
require 'plain'
require 'parallel'

# Defines the conceptual entities of the plain framework.
# The following concepts play a major role and are defined in a cloud service provider agnostic way:
#
# - {Concept::Cluster} represents the cluster which is composed as a list of deployments.
# - {Concept::Deployment} A deployment is a set of nodes being deployed in a district performing a role for the cluster.
# - {Concept::District} represents a region normally provided by a cloud service provider.
#   A district is the provider agnostic concept of a datacenter, a region, a zone or whatever concept
#   a cloud service provider is using to organize its internal infrastructure provisioning.
# - {Concept::Credential} represents credential data (like passwords, identifiers, SSH keys, and so on)
#   to interact with a cloud service provider.
# - {Concept::NodeTemplate} represents a template needed to create a node in district.
#   It maps a provider agnostic role to provider specific concepts like images, machine types,
#   disk definitions, ssh keys and so on.
# - {Provider::Driver} interfaces a cloud service provider's infrastructure.
#   A driver is able to create nodes, to install services on nodes, to start and stop services, and to terminate nodes.
#
# @author Nane Kratzke
#
module Concept

  # Base class for all concepts.
  # @author Nane Kratzke
  #
  class BaseConcept

    attr_reader :data

    # Constructor to create a object.
    # @param data [Hash] object data
    #
    def initialize(data)
      @data = data
    end

    # Returns data for a provided attribute.
    # @return nil if key is not set
    # @return value for the key
    def method_missing(attribute)
      @data["#{attribute}"]
    end

    # Generates a string representation of the object.
    # @return [String] Concept representation
    #
    def to_s
      concept = "#{self.class.name.split('::').last}"
      "#{concept}: #{JSON.pretty_generate(self.data)}"
    end

    # Validates data of the current object.
    # @note This method must be reimplemented by extending classes.
    # @abstract
    # @raise [Exception] If not valid
    # @return [Boolean] <code>true</code>, if valid
    #
    def validate(cluster)
      raise 'Validation error: validate() method must be implemented'
    end

  end

  # Credential data objects hold authentication data for cloud service provider APIs.
  # The credential data is cloud service provider specific.
  # @author Nane Kratzke
  #
  class Credential < BaseConcept

    # Constructor to create a credential object.
    # @param data [Hash] credential data
    #
    def initialize(data)
      super(data)
      self.data['type'] = 'credential'
    end

    # Validates data of the crednetial object.
    # @raise [Exception] If not valid
    # @return [Boolean] <code>true</code>, if valid
    #
    def validate(cluster)
      raise "Object must be of type 'credential'." unless self.type == 'credential'
      raise 'Object must have a provider attribute.' if self.provider.nil?
      true
    rescue Exception => ex
      error = "Credential validation error: #{ex}\nObject: #{self}"
      Plain::log.error(error)
      raise(error)
    end

  end

  # A {Flavor} maps a generic machine category (like small, medium, large)
  # to a provider (and maybe district) specific host configuration
  # necessary to launch a node. Such settings are provider specific and could be:
  #
  # - provider specific machine type,
  # - provider specific image,
  # - provider specific settings to define disk and memory sizes
  # - and so on
  #
  # @author Nane Kratzke
  #
  class Flavor < BaseConcept

    # Constructor to create a {Flavor} object.
    # @param data [Hash] template data
    #
    def initialize(data)
      super(data)
      self.data['type'] = 'flavor'
    end

    # Validates data of the flavor object.
    # @raise [Exception] If not valid
    # @return [Boolean] <code>true</code>, if valid
    #
    def validate(cluster)
      raise "Object must be of type 'flavor'." unless self.type == 'flavor'
      raise "Object must have a flavor attribute." if self.flavor.nil?
      raise "Flavor must be one of: #{cluster.flavors * ', '}" unless cluster.flavors.include?(self.flavor)
      true
    rescue Exception => ex
      error = "Flavor validation error: #{ex}\nObject: #{self}"
      Plain::log.error(error)
      raise(error)
    end
  end

  # A {Deployment} is a set of nodes which have to be deployed in a district.
  # Each node of a deployment performs the same role for a cluster.
  # @author Nane Kratzke
  #
  # @!attribute [r] district
  #   @return [Concept::District] District object responsible to handle the deployment
  # @!attribute [r] role
  #   @return [String] Role each of the nodes to deploy has to fulfill
  # @!attribute [r] flavor
  #   @return [String] Flavor for each of the nodes to deploy
  # @!attribute [r] quantity
  #   @return [Integer] Number of nodes to deploy
  #
  class Deployment < BaseConcept

    attr_reader :district
    #attr_reader :role
    #attr_reader :flavor
    #attr_reader :quantity

    # Constructor to create a deployment object.
    # @param data [Hash] deployment data
    #
    def initialize(data, districts)
      super(data)
      self.data['type'] = 'deployment'
      @district = districts.select { |d| d.id == data['district'] }.first
      #@role = self.data['role'].downcase
      #@flavor = self.data['flavor'].downcase
      #@quantity = self.data['quantity']
    end

    # Validates data of the deployment object.
    # @raise [Exception] If not valid
    # @return [Boolean] <code>true</code>, if valid
    #
    def validate(cluster)
      raise "Object must be of type 'deployment'." unless self.type == 'deployment'
      raise "Object must have a district attribute." if self.district.nil?
      raise "Object must have a flavor attribute." if self.flavor.nil?
      raise "Flavor must be one of: #{cluster.flavors * ', '}" unless cluster.flavors.include?(self.flavor)
      raise "Object must have a role attribute." if self.role.nil?
      raise "Object must be one of: #{cluster.roles * ', '}" unless cluster.roles.include?(self.role)
      raise "Object must have a quantity attribute" if self.quantity.nil?
      raise "Quantity must be greater or equal zero" unless self.quantity >= 0
      self.district.validate(cluster)
      true
    rescue Exception => ex
      error = "Deployment validation error: #{ex}\nObject: #{self}"
      Plain::log.error(error)
      raise(error)
    end

  end

  # District is the provider agnostic concept that public cloud service providers call region, zone, datacenter or alike.
  # A {District} is provided by a cloud service provider.
  # A cluster can be deployed to one or more districts.
  # {Resource Resources} of a cluster are operated in districts.
  #
  # @!attribute [r] id
  #   @return [String] Unique identifier of the district
  # @!attribute [r] credential
  #   @return [Concept::Credential] Credential data associated with the district
  # @!attribute [r] driver
  #   @return [Provider::Driver] Driver object responsible to handle all interactions with the cloud service provider.
  # @!attribute [r] templates
  #   @return [Array<Concept::Flavor>] List of node templates associated with this district.
  #
  # @author Nane Kratzke
  #
  class District < BaseConcept

    attr_reader :credential
    attr_reader :driver
    attr_reader :templates

    def initialize(data, credentials, platform: Platform::Swarm::PLATFORM_ID)
      super(data)
      self.data['type'] = 'district'

      @credential = credentials.select { |c| c.id == data['credential_id'] }.first
      @templates  = self.data['flavors'].map { |n| Flavor.new(n) }
      @driver     = Plain::getProviderDriver(self.provider, self, platform: platform)
    end

    # Returns a {Concept::Flavor} for a role.
    # @return [Concept::Flavor] template object
    #
    def template(flavor)
      Flavor.new(self.data['flavors'].select { |n| n['flavor'] == flavor }.first)
    end

    # Validates data of the district object.
    # @raise [Exception] If not valid
    # @return [Boolean] <code>true</code>, if valid
    #
    def validate(cluster)
      raise "Object must be of type 'district'." unless self.type == 'district'
      raise "Object must have an id." if self.id.nil?
      raise "Object must have a 'provider' attribute." if self.provider.nil?
      raise "No driver for provider '#{self.provider}'." if Plain::getProviderDriver(self.provider, self, platform: cluster.platform).nil?
      raise "No valid driver for provider '#{self.provider}'." unless Plain::getProviderDriver(self.provider, self, platform: cluster.platform).is_a? Provider::Driver
      raise "Object must have a 'credential_id' attribute." if self.credential_id.nil?
      raise "Object must define flavors" if self.flavors.nil?
      myflavors = self.flavors.map { |f| f['flavor'] }
      raise "Flavors #{(cluster.flavors - myflavors) * ', '} not defined." unless cluster.flavors.to_set <=> myflavors.to_set
      self.templates.each { |t| t.validate(cluster) }
      raise "Object must reference a 'credential' object." if self.credential.nil?
      raise "Object must reference a 'credential' object." unless self.credential.is_a? Concept::Credential
      raise "Object must reference a 'driver'." if self.driver.nil?
      raise "Object must reference a 'driver'." unless self.driver.is_a? Provider::Driver
      self.driver.validate(cluster)
      true
    rescue Exception => ex
      error = "District validation error: #{ex}\nObject: #{self}"
      Plain::log.error(error)
      raise(error)
    end

    # Returns the security group for a district.
    # @param [Concept::Cluster] cluster Cluster object
    # @return [Concept::SecurityGroup] secgroup object of the district
    #
    def secgroup(cluster)
      cluster.secgroups.select { |sg| sg.district_id == self.id }.first
    end

  end

  class Resource < BaseConcept

    def initialize(data)
      super(data)
      self.data['type'] = 'resource' if self.data['type'] == nil
    end

    # Validates data of the resource object.
    # @raise [Exception] If not valid
    # @return [Boolean] <code>true</code>, if valid
    #
    def validate(cluster)
      raise "Object must be of type 'resource' or 'node' or 'secgroup'." unless ['node', 'resource', 'secgroup'].include?(self.type)
    rescue Exception => ex
      error = "Resource validation error: #{ex}\nObject: #{self}"
      Plain::log.error(error)
      raise(error)
    end

  end

  class Node < Resource

    def initialize(data)
      super(data)
      self.data['type'] = 'node'
      self.data['tags'] = {
          'district' => self.data['district_id']
      }
      #@id = self.data['id']
      #@role = self.data['role']
      #@public_ip = self.data['public_ip']
      #@private_ip = self.data['internal_ip']
      #@user = self.data['user']
      #@sshkey = self.data['sshkey']
      #@district_id = self.data['district_id']
    end

    # Validates data of the node object.
    # @raise [Exception] If not valid
    # @return [Boolean] <code>true</code>, if valid
    #
    def validate(cluster)
      raise "Object must have a 'district_id' attribute." if self.district_id.nil?
      raise "Cluster must know the 'district_id'." if cluster.district(self.district_id).nil?
      raise "Cluster must know the 'district_id'." unless cluster.district(self.district_id).is_a? Concept::District
      raise "Object must be of type 'node'." unless self.type == 'node'
      raise "Object must have an 'id'." if self.id.nil?
      raise "Object must have tags." if self.tags.nil?
      raise "Object must be tagged" if self.tags['district'].nil?
      raise "Object must be correctly district tagged." unless self.tags['district'] == self.district_id
      raise "Object id must of type String." unless self.id.is_a? String
      raise "Object id must not be empty." unless self.id.empty?
      raise "Object id must be known by cluster." unless cluster.nodes.map { |node| node.id }.contains(self.id)
      raise "Object must have an 'role'." unless self.role.nil?
      raise "Role must be defined by cluster." unless cluster.roles.contains(self.role)
      raise "Object must have a 'public ip'." if self.public_ip.nil?
      raise "Object must have a valid 'public ip'. '#{self.public_ip}' is not valid." unless IPAddr.new(self.public_ip).ipv4?
      raise "Object must have a 'private ip'." if self.private_ip.nil?
      raise "Object must have a valid 'private ip'. '#{self.private_ip}' is not valid." unless IPAddr.new(self.private_ip).ipv4?
      raise "Object must have a 'user'." if self.user.nil?
      raise "User must be the same as specified by the cluster." unless self.user == cluster.user
      raise "Object must have a 'sshkey'." if self.sshkey.nil?
      raise "'sshkey' must be the same as specified by the cluster." unless self.sshkey == cluster.sshkey
    rescue Exception => ex
      error = "Node resource validation error: #{ex}\nObject: #{self}"
      Plain::log.error(error)
      raise(error)
    end

  end

  # A {SecurityGroup} is a ...
  # @author Nane Kratzke
  # @todo
  #
  class SecurityGroup < Resource

    def initialize(data)
      super(data)
      self.data['type'] = 'secgroup'
    end

    # Validates data of the securitygroup resource.
    # @param [Concept::Cluster] cluster Cluster object against data is validated
    # @return [Boolean] <code>true</code>, if everything is valid
    # @return [Boolean] <code>false</code>, otherwise
    #
    # @todo We need a validate method for every model concept!!!
    #
    def validate(cluster)
      raise "A security group must be of type 'secgroup'" unless self.type = 'secgroup'
      raise "A security group must have a 'district_id'." if self.district_id.nil?
      raise "Cluster must know the 'district_id'." if cluster.district(self.district_id).nil?
      raise "Cluster must know the 'district_id'." unless cluster.district(self.district_id).is_a? Concept::District
      # to be done ...
      true
    rescue Exception => ex
      error = "SecurityGroup resource validation error: #{ex}\nObject: #{self}"
      Plain::log.error(error)
      raise(error)
    end

  end

  # A {Cluster} represents a set of nodes forming a cross provider deployed cluster forming an elastic platform.
  # Nodes are deployed via {Concept::Deployment deployments} and {Concept::Flavor flavors}
  # in {Concept::District districts}.
  # Districts are mapped to cloud service provider specific IaaS infrastructures. These infrastructures are
  # accessed via {Provider::Driver drivers}.
  #
  # @!attribute [r] deployments
  #   @return [Array<Concept::Deployment>] List of {Concept::Deployment deployments} the cluster is composed of.
  # @!attribute [r] resources
  #   @return [Array<Concept::Resource>] List of {Concept::Resource resources} the cluster has requested.
  # @!attribute [r] platform
  #   @return [String] The platform one of (swarm, k8s)
  # @!attribute [r] roles
  #   @return [Array<String>] List of roles.
  # @!attribute [r] flavors
  #   @return [Array<String>] List of flavors.
  # @!attribute [r] secret
  #   @return [String] Secret of the cluster. Every node joining the cluster must know this secret. It is not stored in the cluster.
  # @!attribute [r] id
  #   @return [String] Unique identifier of the cluster.
  # @!attribute [r] user
  #   @return [String] Admin user (with sudo right) for each node of the cluster
  # @!attribute [r] keyfile
  #   @return [String] SSH secret key for the admin user for each of the nodes of the cluster
  # @!attribute [rw] ips
  #   @return [String] All public ips of nodes forming the cluster.
  # @!attribute [r] resources_mutex
  #   @return [Mutex] Mutex to use resources in a thread safe way
  #
  # @author Nane Kratzke
  #
  class Cluster < BaseConcept

    attr_reader :deployments
    attr_reader :resources
    attr_reader :platform
    attr_reader :roles
    attr_reader :secret
    attr_reader :id
    attr_reader :user
    attr_reader :keyfile
    attr_reader :ips

    attr_reader :resources_mutex

    # Constructor to create a cluster object.
    # @param [Hash] data cluster definition settings
    # @param [Array<Concept::District>] districts List of districts the cluster can be deployed to
    # @param [Array<Concept::Resource>] resources List of resources the cluster has requested
    #
    def initialize(data, districts, resources)
      super(data)
      self.data['type'] = 'cluster'
      @roles = ['master', 'worker']
      @deployments = self.data['deployments'].map { |d| Deployment.new(d, districts) }
      @resources = resources
      @platform = self.data['platform']
      @secret = self.data['secret']
      @id = self.data['id']
      @user = self.data['user']
      @keyfile = self.data['keyfile']

      @resources_mutex = Mutex.new
      @ips = self.nodes.map { |node| node.public_ip }

    end

    def self.template(name, platform: 'swarm')
      district_ids = Plain::drivers(Plain::options).map { |driver| driver.district_templates(Plain::options) }
                                                   .flatten
                                                   .map { |district| district['id'] }
      {
          "type" => "cluster",
          "platform" => platform,
          'id' => "#{name}",
          "user" => "ubuntu",
          "keyfile" => "sshkey.pem",
          "flavors" => ["small", "medium", "large"],
          "deployments" => district_ids.map { |id|
            [{
                'district' => id,
                'flavor' => 'small',
                'role' => 'master',
                'quantity' => 0
            },
            {
                'district' => id,
                'flavor' => 'small',
                'role' => 'worker',
                'quantity' => 0
            }]
          }.flatten
      }
    end

    # Validates data of the cluster object.
    # @raise [Exception] If not valid
    # @return [Boolean] <code>true</code>, if valid
    #
    def validate(cluster)
      raise "A cluster must be of type 'cluster'." unless self.type == 'cluster'
      raise "A cluster must have a platform." if self.platform.nil?
      raise "A cluster must have an 'id'." if self.id.nil?
      raise "A cluster must have a valid 'id'." if self.id.empty?
      raise "A cluster must define roles." if self.roles.nil?
      raise "A cluster must define roles." if self.roles.empty?
      raise "A cluster must define flavors." if self.flavors.nil?
      raise "A cluster must define flavors." if self.flavors.empty?
      raise "A cluster must have a 'user'." if self.user.nil?
      raise "A cluster must have a valid 'user'." if self.user.empty?
      raise "A cluster must have a 'keyfile'." if self.keyfile.nil?
      raise "Private part of 'keyfile' (#{self.keyfile}) must be present." unless File.file?(self.keyfile)
      raise "Public part of 'keyfile' (#{self.keyfile}.pub) must be present." unless File.file?("#{self.keyfile}.pub")
      self.resources.each { |r| r.validate(cluster) }
      self.deployments.each { |d| d.validate(cluster) }
      # to be continued
      true
    rescue Exception => ex
      error = "Cluster validation error: #{ex}\nObject: #{self}"
      Plain::log.error(error)
      raise error
    end

    # Returns a specified {Concept::District district}.
    # @param [String] id Identifier of the district
    # @return [Concept::District] District object with identifier <code>id</code>
    #
    def district(id)
      self.deployments.map { |d| d.district }.select { |d| d.id == id }.first
    end

    # Returns all {Concept::District districts}.
    # @return [Array<Concept::District>] List of district objects
    #
    def districts
      self.deployments.map { |d| d.district }.uniq
    end

    # Writes current state of resources to resources file provided via --resources command line option.
    #
    def write_resources
      File.write(Plain::options.resources, JSON.pretty_generate(self.resources.map { |r| r.data }))
    end

    # Returns all {Concept::Node nodes} of the cluster.
    # @return [Array<Concept::Node>] List of node objects
    #
    def nodes
      ns = []
      self.resources_mutex.synchronize do
        ns = self.resources.select { |r| r.type == 'node' }
      end
      ns
    end

    # Returns all {Concept::Node master} nodes of the cluster.
    # @return [Array<Concept::Node>] List of master nodes
    #
    def masters
      ms = []
      self.resources_mutex.synchronize do
        ms = self.resources.select { |r| r.type == 'node' && r.role == 'master' }
      end
      ms
    end

    # Returns all {Concept::Node worker} nodes of the cluster.
    # @return [Array<Concept::Node>] List of worker nodes
    #
    def workers
      ws = []
      self.resources_mutex.synchronize do
        ws = self.resources.select { |r| r.type == 'node' && r.role == 'worker' }
      end
      ws
    end

    # Returns all {Concept::SecurityGroup security groups} of the cluster.
    # @return [Array<Concept::SecurityGroup>] List of secgroup objects
    #
    def secgroups
      sgs = []
      self.resources_mutex.synchronize do
        sgs = self.resources.select { |r| r.type == 'secgroup' }
      end
      sgs
    end

    def nodes_with_role_and_flavor(district_id, role, flavor)
      self.nodes.select { |n| n.district_id == district_id && n.role == role && n.flavor == flavor }
    end

    # Determines necessary tasks to get cluster into desired state.
    # Each task is a hash of the following form:
    #   {
    #     'action'   => STRING  (one of 'create' or 'delete'),
    #     'resource' => STRING  (one of 'node' or 'secgroup'),
    #     'district' => STRING  (district id),
    #     'priority' => INTEGER (positive value in [0..1000]),
    #     'role'     => STRING  (one of 'master' or 'worker', only if resource == 'node'),
    #     'flavor'   => STRING  (one of defined flavors, e.g. 'small', 'medium' or 'large', only if resource == 'node'),
    #     'parallel' => BOOLEAN (if tasks of same resource, action and priority can be executed in parallel)
    #   }
    # @return [Array<Hash>] Returns a list of tasks (an action plan composed of tasks as described above).
    #
    def actions
      needed_districts  = self.deployments.select { |d| d.quantity > 0 }.map { |d| d.district.id }.uniq
      current_districts = self.secgroups.map { |sg| sg.district_id }.uniq

      tasks = []
      (needed_districts - current_districts).each do |district_id|
        tasks << {
            'action'   => 'create',
            'resource' => 'secgroup',
            'district' => district_id,
            'priority' => 1000,
            'parallel' => true
        }
      end

      (current_districts - needed_districts).each do |district_id|
        tasks << {
            'action'   => 'delete',
            'resource' => 'secgroup',
            'district' => district_id,
            'priority' => 0,
            'parallel' => true
        }
      end

      self.deployments.each do |deployment|
        district = deployment.district.id
        quantity = deployment.quantity
        role     = deployment.role
        flavor   = deployment.flavor
        tags     = deployment.tags.nil? ?  {} : deployment.tags

        founds = self.nodes_with_role_and_flavor(district, role, flavor)
        delta = quantity - founds.count

        priority = 100
        priority = 750 if delta > 0 && role == 'master'
        priority = 50  if delta < 0 && role == 'master'
        priority = 250 if delta > 0 && role == 'worker'

        (1..delta.abs).each do |i|
          tasks << {
              'action'   => delta < 0 ? 'delete' : 'create',
              'resource' => 'node',
              'district' => district,
              'priority' => priority,
              'role'     => role,
              'flavor'   => flavor,
              'tags'     => tags,
              'parallel' => role == 'worker' && delta > 0
          }
        end

      end

      tasks.sort { |t1, t2| t2['priority'] <=> t1['priority'] }
    end

    def request_node(task)
      Plain::log.debug("Processing Cluster::request_node with task #{task}")

      district_id = task['district']
      driver = self.district(district_id).driver
      driver.check_and_configure

      node = driver.request_node(task['flavor'], task['role'], self)
      Plain::log.debug("Node successfully created: #{node}")

      node.tags.merge!(task['tags'])

      Retryable.retryable(tries: 6, sleep: 5) { driver.install_node(node) }
      self.ips << node.public_ip
      node
    end

    def join_node(node)
      Plain::log.debug("Processing Cluster::join_node with node #{node}")

      district_id = node.district_id
      driver = self.district(district_id).driver
      driver.check_and_configure

      driver.join_node(node, self)
      self.resources_mutex.synchronize do
        self.resources << node
        self.write_resources
      end

      node
    end

    # Deletes and removes an existing node of the cluster.
    # @param [Hash] task task description for delete
    # @see Cluster::actions
    # @return [Concept::Node|Concept::Resource] deleted node object, might be <code>nil</code> in case of failure
    #
    def delete_node(task)
      Plain::log.debug("Deleting node because of task #{task}")

      district_id = task['district']
      driver = self.district(district_id).driver
      driver.check_and_configure

      # We could take the oldest one here ...
      node = self.nodes_with_role_and_flavor(district_id, task['role'], task['flavor']).sample
      Plain::log.debug("Deleting node #{node}")
      self.resources_mutex.synchronize do
        self.resources.delete(node)
        self.write_resources
      end

      self.ips.delete(node.public_ip)
      driver.leave(node, self)
      driver.terminate_node(node)

      node
    end

    # Creates a new security group for the cluster.
    # @param [Hash] task task description
    # @see Cluster::actions
    # @return [Concept::SecurityGroup|Concept::Resource] secgroup object, might be <code>nil</code> in case of failure
    #
    def create_secgroup(task)
      district_id = task['district']
      driver = self.district(district_id).driver
      driver.check_and_configure

      secgroup = driver.create_secgroup(self)

      self.resources_mutex.synchronize do
        self.resources << secgroup
        self.write_resources
      end

      secgroup
    end

    # Deletes and removes an existing security group from a district of the cluster.
    # @param [Hash] task task description
    # @see Cluster::actions
    # @return [Concept::SecurityGroup|Concept::Resource] deleted security group object
    #
    def delete_secgroup(task)
      district_id = task['district']
      district = self.district(district_id)
      driver = district.driver
      driver.check_and_configure

      secgroup = district.secgroup(self)
      driver.terminate_secgroup(secgroup)
      self.resources_mutex.synchronize do
        self.resources.delete(secgroup)
        self.write_resources
      end

      secgroup
    end

    def adaption_loop2(progress, steps: [])

      timeline = Plain::trace_timeline()

      tasks = steps.group_by { |action| action['priority'] }
      prios = tasks.keys.sort.reverse

      for prio in prios

        seq_tasks = tasks[prio].select { |action| not action['parallel'] }
        par_tasks = tasks[prio].select { |action| action['parallel'] }

        nodes_changed = false

        for task in seq_tasks
          begin
            if task['action'] == 'create' && task['resource'] == 'secgroup'
              Plain::add_event_to_timeline(task, timeline) { create_secgroup(task) }
            end

            if task['action'] == 'delete' && task['resource'] == 'secgroup'
              Plain::add_event_to_timeline(task, timeline) { delete_secgroup(task) }
            end

            if task['action'] == 'create' && task['resource'] == 'node'
              req_node_task = task.clone
              req_node_task['action'] = 'request'

              node = nil
              Plain::add_event_to_timeline(req_node_task, timeline) { node = self.request_node(task)  }
              self.sync_secgroups(timeline)
              join_node_task = task.clone
              join_node_task['action'] = 'join'
              Plain::add_event_to_timeline(join_node_task, timeline) { self.join_node(node) }
            end

            if task['action'] == 'delete' && task['resource'] == 'node'
              Plain::add_event_to_timeline(task, timeline) { self.delete_node(task) }
              nodes_changed = true
            end

            progress.increment unless progress.nil?
          rescue Exception => ex
            Plain::log.error("#{ex}")
            puts "#{ex.backtrace * ', '}"
            abort("Aborting scaling loop due to #{ex}")
          end
        end

        new_nodes = []
        delayed_join_progress = 0
        delayed_sync_progress = 0
        errors = []

        Parallel.each(par_tasks, in_threads: par_tasks.count) do |task|
          begin

            if task['action'] == 'create' && task['resource'] == 'secgroup'
              Plain::add_event_to_timeline(task, timeline) { create_secgroup(task) }
            end

            if task['action'] == 'delete' && task['resource'] == 'secgroup'
              Plain::add_event_to_timeline(task, timeline) { delete_secgroup(task) }
            end

            if task['action'] == 'create' && task['resource'] == 'node'
              req_node_task = task.clone
              req_node_task['action'] = 'request'
              Plain::add_event_to_timeline(req_node_task, timeline) { new_nodes << self.request_node(task) }
              nodes_changed = true
              delayed_join_progress += 1
            end

            if task['action'] == 'delete' && task['resource'] == 'node'
              Plain::add_event_to_timeline(task, timeline) { self.delete_node(task) }
              nodes_changed = true
              delayed_sync_progress += 1
            end

            delayed = (delayed_join_progress > 0 or delayed_sync_progress > 0)

            progress.increment unless progress.nil? or delayed
          rescue Exception => ex
            puts "#{ex.backtrace * ', '}"
            Plain::log.error("#{ex}")
            errors << "#{ex}"
          end
        end

        if nodes_changed
          self.sync_secgroups(timeline)
          delayed_sync_progress.times { progress.increment unless progress.nil? }
          for node in new_nodes.compact
            task = {
                'action' => 'join',
                'resource' => 'node',
                'district' => node.district_id
            }
            Plain::add_event_to_timeline(task, timeline) { self.join_node(node) }
            progress.increment unless progress.nil?
          end
        end

        unless errors.empty?
          abort("Aborting scaling loop. Following errors occured:\n #{errors * "\n" }")
        end
      end

      Plain::close_timeline(timeline)
    end

    # Adjusts all security groups and provides access to public IP addresses
    # of each node of the cluster.
    #
    def sync_secgroups(timeline)
      Parallel.each(self.districts, in_threads: self.districts.count) do |district|
        secgroup = district.secgroup(self)
        task = {
            'action' => 'adjust',
            'resource' => 'secgroup',
            'district' => district.id
        }
        unless secgroup.nil?
          Plain::add_event_to_timeline(task, timeline) { district.driver.update_secgroup(secgroup, self, self.ips) }
        end
      end
      #sleep(10) # Maybe we should push this to the drivers?
    end
  end
end