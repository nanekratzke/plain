module Platform

  # Defines the driver capable to deploy Kubernetes elastic platforms.
  # @author Nane Kratzke
  #
  class K8S < Platform::Driver

    K8S_TOKENS_FILE = 'k8s.tokens'

    PLATFORM_ID = 'k8s'

    # Welcome login message on a K8S cluster.
    #
    def welcome
      k8s = "Kubernetes".yellow
      cluster_info = "kubectl cluster-info".yellow
      list_nodes = "kubectl get nodes".yellow
      payload = "kubectl get pods".yellow
      allpods = "kubectl get pods --all-namespaces".yellow

      [
          "",
          "Your #{k8s} cluster is up and running.",
          "Execute one of the following commands to get started:",
          "- #{cluster_info} to get basic informations about your cluster",
          "- #{list_nodes} to list all nodes forming your cluster",
          "- #{payload} to list running pods",
          "- #{allpods} to list all pods (this will include K8S system pods)",
          "",
          "Connecting ... (standby)",
          ""
      ] * "\n"
    end

    def cluster_state(cluster)
      cmd = "kubectl get nodes -o json"
      master = cluster.masters.sample
      exit, json, stderr = Open4ssh.capture3(
          host: master.public_ip,
          user: master.user,
          key:  master.sshkey,
          cmd: cmd
      )
      raise("Command '#{cmd}' failed (#{stderr}.") unless exit == 0
      data = JSON.parse(json)

      nodes = data['items'].select { |entry| entry['kind'] == 'Node' }
      nodes.map { |n|
          conditions = {}
          n['status']['conditions'].each { |c| conditions[c['type']] = c['status'] }
          {
            'name' => n['metadata']['name'],
            'cpu' => n['status']['allocatable']['cpu'],
            'memory' => n['status']['allocatable']['memory'],
            'pods' => n['status']['allocatable']['pods'],
            'ready' => conditions['Ready'],
            'disk_pressure' => conditions['DiskPressure'],
            'memory_pressure' => conditions['MemoryPressure'],
            'out_of_disk' => conditions['OutOfDisk']
          }
      }
    end

    # Installs and configures necessary software on a node.
    # @note Should only be called after successful {Provider::Driver.request_node} call.
    #
    def install_node(node)
      Plain::log.info("Installing #{node.id} started.")

      # Lets figure out the nic name
      exit, stdout, stderr = Open4ssh.capture3(
          host: node.public_ip,
          user: node.user,
          key:  node.sshkey,
          cmd:  "ls /sys/class/net"
      )
      raise ("Could not figure out network interface (#{stderr})") unless exit == 0
      net = stdout.tr("lo", "").strip
      Plain::log.info("Configuring network interface #{net} of #{node.id} to work with public ip #{node.public_ip}.")

      cmds = [
          "sudo ifconfig #{net}:0 #{node.public_ip} netmask 255.255.255.255",
      ]
      returns = Open4ssh.capture4(
          host: node.public_ip,
          user: node.user,
          key:  node.sshkey,
          cmd:  cmds
      )
      raise("Could not configure network interface #{net} of #{node.id} to work with public ip #{node.public_ip} (#{Open4ssh.console(returns)})") unless Open4ssh.success(returns)

      labels = node.tags.map { |k, v| "#{k}=#{v}"} * ","

      cmds = [
        "echo 127.0.0.1 $(hostname) | sudo tee -a /etc/hosts",
        "curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -",
        "sudo sh -c 'echo deb http://apt.kubernetes.io/ kubernetes-xenial main > /etc/apt/sources.list.d/kubernetes.list'",
        "sudo apt-get update",
        # "sudo apt-get upgrade -y",
        "sudo apt-get install -y docker.io kubelet kubeadm kubectl kubernetes-cni",
        "sudo sed -i '1 a Environment=\"KUBELET_EXTRA_ARGS=--node-ip=#{node.public_ip} --node-labels=#{labels}\"' /etc/systemd/system/kubelet.service.d/10-kubeadm.conf",
        "sudo systemctl daemon-reload",
        # "git clone https://github.com/kubernetes/heapster.git"
      ]

      Plain::log.info("Installing Kubernetes and add-ons on node #{node.id} (#{node.public_ip}). This may take some time ...")
      returns = Open4ssh.capture4(
          host: node.public_ip,
          user: node.user,
          key:  node.sshkey,
          cmd:  cmds
      )
      raise("Could not install node #{node.id} (#{Open4ssh.console(returns)})") unless Open4ssh.success(returns)
      Plain::log.info("Installed Kubernetes on #{node.id} (#{node.public_ip}) successfully.")

    rescue Exception => ex
      error = "Install of Kubernetes on node #{node.id} failed due to #{ex}."
      Plain::log.error(error)
      raise(error)
    end

    # Calling this method causes the node to join a cluster.
    # If this is the first node this will init the cluster.
    # @note Should only be called after successful {Provider::Driver.install_node} call.
    #
    def join_node(node, cluster)

      Plain::log.debug("#{node}")

      if cluster.masters.count == 0
        # We are the first one!
        Plain::log.info("Node #{node.id} initializes the cluster.")
        raise("Can not start a cluster with a worker node (#{node}).") unless node.role == 'master'
        exit, stdout, stderr = Open4ssh.capture3(
            host: node.public_ip,
            user: node.user,
            key:  node.sshkey,
            cmd:  "kubeadm token generate"
        )

        if exit == 0
          # Get the token from output
          Plain::log.debug("#{stdout}#{stderr}")
          token = stdout.strip
          Plain::log.debug("Token: #{token}")
          File.write(K8S_TOKENS_FILE, token)

          cmds = []
          cmds << "sudo kubeadm init --skip-preflight-checks --token=#{token} --apiserver-advertise-address=#{node.public_ip}"
          cmds << "sudo cp /etc/kubernetes/admin.conf $HOME/"
          cmds << "sudo chown $(id -u):$(id -g) $HOME/admin.conf"
          cmds << "kubectl --kubeconfig=$HOME/admin.conf taint nodes --all node-role.kubernetes.io/master-"
          cmds << "kubectl --kubeconfig=$HOME/admin.conf apply -f https://git.io/weave-kube-1.6"
          # cmds << "kubectl --kubeconfig=$HOME/admin.conf apply -f heapster/deploy/kube-config/influxdb"
          cmds << "echo 'export KUBECONFIG=$HOME/admin.conf' >> .profile"

          Plain::log.debug("Node #{node.id} initializes the cluster ... this may take some time.")
          returns = Open4ssh.capture4(
              host: node.public_ip,
              user: node.user,
              key:  node.sshkey,
              cmd:  cmds
          )

          Plain::log.debug(Open4ssh.console(returns))
          raise("Node #{node.id} could not (fully) initialize the cluster. (#{Open4ssh.console(returns)})") unless Open4ssh.success(returns)

        else
          raise("#{stdout}#{stderr}")
        end
        Plain::log.info("K8S initialized successfully by node #{node.id}. Tokens written to '#{K8S_TOKENS_FILE}'")
      else
        master = cluster.masters.sample
        Plain::log.info("Node #{node.id} is going to join the cluster as #{node.role} via master #{master.id}.")
        token = File.read(K8S_TOKENS_FILE).strip

        exit, stdout, stderr = Open4ssh.capture3(
            host: node.public_ip,
            user: node.user,
            key:  node.sshkey,
            cmd:  "sudo kubeadm join --skip-preflight-checks --token=#{token} #{master.public_ip}:6443"
        )

        Plain::log.error("#{stdout}#{stderr}") unless exit == 0
        raise("K8S join failed of node #{node.id} via master #{master.id} (#{stderr})") unless exit == 0

        Plain::log.info("Node #{node.id} joined the cluster as #{node.role} via master #{master.id} successfully.")
      end
    rescue Exception => ex
      error = "Error while joining node '#{node.id}' due to '#{ex}'"
      Plain::log.error (error)
      raise(error)
    end

    # Calling this method causes the node to be drained.
    # All pods are rescheduled to other scheduleable nodes.
    # Node is marked as unscheduled.
    #
    def drain_node(node, cluster)

      # TODO
      # 1. We have to drain the node (https://kubernetes.io/docs/tasks/administer-cluster/safely-drain-node/)
      # 2. We have to deregister the node (kubectl delete node)

    end

    # Calling this method causes the node to leave the cluster.
    #
    def leave(node, cluster)
      if cluster.masters.count == 1 && cluster.nodes.count > 1 && node.role == 'master'
        raise("Node #{node.id} can not leave the cluster (there are still workers).")
      end

      self.drain_node(node, cluster)

      Plain::log.debug("Node #{node.id} leaves the cluster.")

      exit, stdout, stderr = Open4ssh.capture3(
          host: node.public_ip,
          user: node.user,
          key:  node.sshkey,
          cmd:  "sudo kubeadm reset"
      )

      Plain::log.info("#{node.role} node #{node.id} left the cluster successfully.")
      raise("Node #{node.id} could not leave the cluster. (#{stdout}#{stderr})") unless exit == 0
    rescue Exception => ex
      error = "Error while leaving node '#{node.id}' due to '#{ex}'"
      Plain::log.error(error)
      raise(error)
    end
  end
end