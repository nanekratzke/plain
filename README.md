
# Plain

Plain is a Ruby command-line tool to deploy, scale, and even transfer elastic container platforms like Kubernetes, Docker Swarm,
Mesos, and more in multi-cloud contexts at runtime. It adapts the descriptive _intended state model_ known from Kubernetes but applies
it to the container platform itself. Other than workflow based approaches like TOSCA, an operator simply defines the intended state of the platform and Plain tries to
reach this intended state.

It follows common Ruby gem conventions. Therefore, it can be installed via `rake install`.

Plain supports on the infrastructure level the following IaaS providers 

- AWS
- Azure
- Google Cloud
- OpenStack

and on the platform level the following elastic container platforms:

- Kubernetes
- Docker Swarm

However, Plain is designed for extensibility in mind. So, further infrastructures and container platforms
can be extended by providing additional infrastructure or platform drivers.

To support this, Plain is intensively documented via YARD inline comments. Therefore, we refer to the inline documentation for further information.

## Corresponding research papers

Plain has been developed as a research prototype for the research project Cloud TRANSIT to demonstrate that it is possible to transfer
elastic container platforms like Kubernetes and Docker Swarm at runtime without downtime between different cloud service providers.
The following papers deal with specific aspects:

- [About the Complexity to Transfer Cloud Applications at Runtime and how Container Platforms can Contribute?](https://www.researchgate.net/publication/328134271) PREPRINT!
- [Smuggling Multi-Cloud Support into Cloud-native Applications using Elastic Container Platforms](https://www.researchgate.net/publication/313860386)

The presented approach might be even used for __moving target defences__ on infrastructure level.
The following two position papers deal with this aspect:

- [About being the Tortoise or the Hare? A Position Paper on Making Cloud Applications too Fast and Furious for Attackers](https://www.researchgate.net/publication/323141909)
- [About an Immune System Understanding for Cloud-native Applications - Biology Inspired Thoughts to Immunize the Cloud Forensic Trail](https://www.researchgate.net/publication/322577384)

An extended overview of the research project Cloud TRANSIT can be found in the corresponding [technical report](https://www.researchgate.net/publication/327281981).

## Contributing

Bug reports and pull requests are welcome on BitBucket at https://bitbucket.org/nanekratzke/plain.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

