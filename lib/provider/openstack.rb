require 'fog/openstack'
require 'parallel'

module Provider

  # OpenStack specific driver code.
  # @author Nane Kratzke
  #
  module OpenStack

    # OpenStack IaaS infrastructure driver.
    # @author Nane Kratzke
    #
    class Driver < Provider::Driver

      PROVIDER_ID = 'openstack'

      OS_ZONE_TOPOLOGY = ['nova']

      def initialize(district, platform)
        super(district, platform)
        @id = PROVIDER_ID
      end

      # Static method
      # @param [] options command line options
      #
      def self.credential_template(options)
        puts("Info: You can provide your tenant ID using --os-tenant-id option") if options.os_tenant_id.nil?
        puts("Info: You can provide your API key using --os-api-key option") if options.os_api_key.nil?
        puts("Info: You can provide your user name using --os-user-name option") if options.os_user_name.nil?
        puts("Info: You can provide your domain using --os-domain option") if options.os_domain.nil?

        tenant = options.os_tenant_id.nil? ? '<TENANT_ID>' : options.os_tenant_id
        user_name = options.os_user_name.nil? ? '<USER_NAME>' : options.os_user_name
        api_key = options.os_api_key.nil? ? '<API_KEY>' : options.os_api_key
        domain = options.os_domain.nil? ? 'default' : options.os_domain

        {
            "type": "credential",
            "id": "#{PROVIDER_ID}_default",
            "provider": PROVIDER_ID,
            "os_tenant_id": tenant,
            "os_user_name": user_name,
            "os_api_key": api_key,
            "os_domain": domain
        }
      end

      # Static method
      # @param [] options command line options
      #
      def self.district_templates(options)
        zones = options.os_zones.nil? ? OS_ZONE_TOPOLOGY : options.os_zones.split(/, */)

        project_name = options.os_project_name.nil? ? "<OS_PROJECT_NAME>" : options.os_project_name
        auth_url = options.os_auth_url.nil? ? "<OS_AUTH_URL>" : options.os_auth_url
        large_flavor = options.os_large_flavor.nil? ? "<OS_LARGE_MEDIUM_FLAVOR>" : options.os_large_flavor
        medium_flavor = options.os_small_flavor.nil? ? "<OS_SMALL_MEDIUM_FLAVOR>" : options.os_medium_flavor
        small_flavor = options.os_small_flavor.nil? ? "<OS_SMALL_FLAVOR>" : options.os_small_flavor
        image_id = options.os_image_id.nil? ? "<OS_IMAGE_ID>" : options.os_image_id
        external_network = options.os_external_network.nil? ? "<OS_EXTERNAL_NETWORK_NAME>" : options.os_external_network

        zones.map do |zone|
          {
              'type' => 'district',
              'id' => "#{PROVIDER_ID}-#{zone}".downcase,
              'provider' => PROVIDER_ID,
              'credential_id' => "#{PROVIDER_ID}_default",
              'os_zone' => zone,
              'os_project' => project_name,
              'os_auth_url' => auth_url,
              'os_external_network' => external_network,
              'flavors' => [
                  {
                      'flavor' => 'small',
                      'os_flavor' => small_flavor,
                      'os_image' => image_id,
                  },
                  {
                      'flavor' => 'medium',
                      'os_flavor' => medium_flavor,
                      'os_image' => image_id,
                  },
                  {
                      'flavor' => 'large',
                      'os_flavor' => large_flavor,
                      'os_image' => image_id,
                  }
              ]
          }
        end
      end

      # Checks whether command line client is installed.
      # @return [Boolean] <code>true</code> always
      #
      def cli_installed?
        true
      end

      def cli_configure
        # not necessary (CLI not used by this driver)
      end

      # Provides help text to install the Azure command line client.
      # @return [String] Help text
      #
      def cli_install_instructions
        "OpenStack command line tools seem to be missing on your system.\n" \
        "However, they are not necessary.\n"
      end

      # Returns compute client for OpenStack.
      # @private
      # @return [Fog::Compute::OpenStack] compute client
      #
      def _os_compute_client()
        Fog::Compute::OpenStack.new({
            openstack_auth_url: self.district.os_auth_url,
            openstack_username: self.credential.os_user_name,
            openstack_api_key: self.credential.os_api_key,
            openstack_project_name: self.district.os_project,
            openstack_domain_name: self.credential.os_domain
        })
      end

      # Returns network client for OpenStack.
      # @private
      # @return [Fog::Network::OpenStack] network client (neutron)
      #
      def _os_network_client()
        Fog::Network::OpenStack.new(
            openstack_auth_url: self.district.os_auth_url,
            openstack_username: self.credential.os_user_name,
            openstack_api_key: self.credential.os_api_key,
            openstack_project_name: self.district.os_project,
            openstack_domain_name: self.credential.os_domain
        )
      end

      # Creates a security group for the cluster. The following detail resources are requested
      # from the OpenStack IaaS Infrastructure for the responsible district of the driver.
      #
      #   1. OpenStack security group
      #   2. Default OpenStack security group rule providing SSH access for every node
      #   3. SSH Key
      #
      # @return [Concept::SecurityGroup] security group object
      #
      def create_secgroup(cluster)

        secgroup_name = "secgroup-#{SecureRandom.hex(7)}"
        key_name = "sshkey-for-#{secgroup_name}"

        key = self._os_compute_client.key_pairs.create(
            name: key_name,
            public_key: File.read("#{cluster.keyfile}.pub")
        )
        Plain::log.info("Uploaded key #{key.id}")

        os_secgroup = self._os_compute_client.security_groups.create(
            name: secgroup_name,
            description: "Security group for cluster #{cluster.id} (plain auto created)"
        )
        Plain::log.info("Created security group #{secgroup_name} (#{os_secgroup.id}) for cluster #{cluster.id}")

        self._os_compute_client.security_group_rules.create(
            parent_group_id: os_secgroup.id,
            ip_protocol: "tcp",
            from_port: 22,
            to_port: 22
        )

        external_network = self._os_network_client.networks.select { |nw| nw.name == self.district.os_external_network }.first
        Plain::log.info("Working with external network #{external_network}  for cluster #{cluster.id}")

        Concept::SecurityGroup.new({
             'id'           => os_secgroup.id,
             'district_id'  => self.district.id,
             'created'      => "#{Time.now}",
             'os_external_network_id' => external_network.id,
             'os_secgroup_name' => secgroup_name,
             'os_secgroup_id' => os_secgroup.id,
             'os_key_name' => key.name
        })
      rescue Exception => ex
        error = "#{self.class.name}: #{ex}"
        puts(ex.backtrace * "\n")
        Plain::log.error(error)
        raise(error)
      end


      def update_secgroup(secgroup, cluster, ips)
        Plain::log.info("Updating #{secgroup.id}")

        os_security_group = self._os_compute_client.security_groups.get(secgroup.os_secgroup_id)

        Plain::log.info("Determining current allowed public IP addresses")
        current = []
        for os_rule in os_security_group.security_group_rules
          current << os_rule.ip_range['cidr'].split("/")[0] unless os_rule.ip_range['cidr'].nil?
        end
        current = current.uniq - ["0.0.0.0"]

        # Revoking access of no longer existing IP addresses
        to_delete = (current - ips)
        Plain::log.info("Revoking access for IPs #{ to_delete * ', ' }")
        Parallel.each(to_delete, in_threads: to_delete.count) do |addr|
          for os_rule in os_security_group.security_group_rules
            os_rule.destroy if os_rule.ip_range['cidr'].split("/")[0] == addr
          end
        end unless to_delete.empty?

        # Allowing access for new IP addresses
        to_add = (ips - current)
        Plain::log.info("Allowing access for IPs #{ to_add * ', ' }")
        Parallel.each(to_add, in_threads: to_add.count) do |addr|
          ipr = { "cidr" => "#{addr}/32 " }

          self._os_compute_client.security_group_rules.create(
              parent_group_id: os_security_group.id,
              ip_protocol: 'tcp',
              from_port: 1,
              to_port: 65535,
              ip_range: ipr
          )

          self._os_compute_client.security_group_rules.create(
              parent_group_id: os_security_group.id,
              ip_protocol: 'udp',
              from_port: 1,
              to_port: 65535,
              ip_range: ipr
          )

          self._os_compute_client.security_group_rules.create(
              parent_group_id: os_security_group.id,
              ip_protocol: 'icmp',
              from_port: 0,
              to_port: 255,
              ip_range: ipr
          )
        end unless to_add.empty?

        Plain::log.info("Updating of secgroup #{secgroup.id} completed")
      rescue Exception => ex
        error = "#{self.class.name}: #{ex}"
        Plain::log.error(error)
        raise(error)
      end

      # Terminates a security group.
      # The following requested resources are released
      # from the OpenStack IaaS Infrastructure for the responsible district of the driver.
      #
      # All resources assigned to the to be terminated security group
      # (initially requested by {Provider::OpenStack::Driver.create_secgroup} method). This includes
      #
      #   1. OpenStack virtual network (with 10.0.0.0/16 and a default subnet of the same address space)
      #   2. OpenStack security group
      #   3. OpenStack router (assigned to the virtual network)
      #   4. SSH key
      #
      # If the method returns without raising an exception the secgroup could be successfully deleted.
      #
      def terminate_secgroup(secgroup)
        Plain::log.info("Terminating OpenStack security group #{secgroup.id}")

        key = self._os_compute_client.key_pairs.get(secgroup.os_key_name)
        security_group = self._os_compute_client.security_groups.get(secgroup.os_secgroup_id)

        unless key.nil?
          key.destroy
          Plain::log.info("Key pair #{key.name} deleted")
        end

        unless security_group.nil?
          Retryable.retryable(:tries => 3, :sleep => 60) do
            security_group.destroy
            Plain::log.info("Security group #{secgroup.id} deleted")
          end
        end
      rescue Exception => ex
        error = "#{self.class.name}: #{ex}"
        Plain::log.error(error)
        raise(error)
      end

      # Terminates a mode and releases
      # - allocated Azure NIC resource
      # - allocated Azure public IP address resource
      #
      # If the method returns without raising an exception the node could be successfully terminated.
      #
      def terminate_node(node)
        Plain::log.info("#{self.class.name}: Terminating #{node.role} node #{node.id} in district #{self.district.id}")
        server = self._os_compute_client.servers.get(node.id)
        Plain::log.info("Terminating node #{server.id}")
        server.destroy
        Plain::log.info("Terminated node #{server.id} in district #{self.district.id}")
      rescue Exception => ex
        error = "#{self.class.name}: #{ex}"
        Plain::log.error(error)
        raise(error)
      end

      def request_node(flavor, role, cluster)
        Plain::log.info("#{self.class.name}: Requesting #{role} node for district #{self.district.id}")

        secgroup = self.district.secgroup(cluster)
        os_secgroup = self._os_compute_client.security_groups.get(secgroup.os_secgroup_id)

        template = self.template(flavor)

        node_name = "node-#{SecureRandom.hex(7)}"

        Plain::log.debug("#{secgroup}")
        Plain::log.debug("#{self.district}")
        Plain::log.info("Requesting node #{node_name}")
        os_node = self._os_compute_client.servers.create(
            name: node_name,
            image_ref: template.os_image,
            zone: self.district.os_zone,
            flavor_ref: template.os_flavor,
            key_name: secgroup.os_key_name,
            nics: [{ :net_id => secgroup.os_external_network_id }],
            security_groups: os_secgroup
        )
        os_node.wait_for { ready? }
        os_node.reload

        Plain::log.debug("#{os_node.ip_addresses}")

        # Wait until ssh able ()
        Retryable.retryable(tries: 6, sleep: 10) do
          exit, stdout, stderr = Open4ssh.capture3(
              host: os_node.ip_addresses.first,
              user: cluster.user,
              key:  cluster.keyfile,
              cmd:  "ls /sys/class/net"
          )
        end

        node = Concept::Node.new({
             'id'          => os_node.id,
             'role'        => role,
             'flavor'      => flavor,
             'public_ip'   => os_node.ip_addresses.first,
             'private_ip'  => os_node.ip_addresses.first,
             'user'        => cluster.user,
             'sshkey'      => cluster.keyfile,
             'district_id' => self.district.id,
             'os_zone'  => self.district.os_zone,
             'created'     => "#{Time.now}",
        })
        Plain::log.debug("#{node}")

        Plain::log.info("#{self.class.name}: Created OpenStack node #{os_node.id} (#{os_node.public_ip_address}) successfully in district #{self.district.id}.")
        node
      rescue Exception => ex
        error = "#{self.class.name}: #{ex} (Stacktrace: #{ex.backtrace * "\n"})"
        Plain::log.error(error)
        raise(error)
      end
    end
  end
end