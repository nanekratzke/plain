require "plain/version"
require 'Open3'
require 'colorize'

require 'model'

require 'provider'
require 'provider/gce'
require 'provider/aws'
require 'provider/azure'
require 'provider/openstack'

require 'platform/swarm'
require 'platform/k8s'

module Plain

  LOCK_FILE = 'PLAIN.lock'
  TIMELINE_FILE = 'timeline.json'

  def self.read_lock
    unless File.exists?(LOCK_FILE)
      f = File.new(LOCK_FILE, 'w+')
      f.flush
      f.close
    end
    f = File.open(LOCK_FILE)
    f.flock(File::LOCK_SH)
  end


  def self.write_lock
    unless File.exists?(LOCK_FILE)
      f = File.new(LOCK_FILE, 'w+')
      f.flush
      f.close
    end
    f = File.open(LOCK_FILE)
    f.flock(File::LOCK_EX)
  end


  def self.unlock
    f = File.open(LOCK_FILE)
    f.flock(File::LOCK_UN)
  end

  # Reads credentials from a credential definition file.
  # @param [String] path Path of a credential definition file
  # @return [Array<Concept::Credential>] List of credentials the cluster can be deployed into
  #
  def self.readCredentials(path)
    json = JSON.parse(File.read(path))
    json.map { |cred| Concept::Credential.new(cred) }
  end

  # Reads districts from a district definition file.
  # @param [String] path Path of district definition file
  # @return [Array<Concept::District>] List of districts the cluster can be deployed into
  #
  def self.readDistricts(path, credentials, platform: Platform::Swarm::PLATFORM_ID)
    json = JSON.parse(File.read(path))
    json.map { |cred| Concept::District.new(cred, credentials, platform: platform) }
  end

  # Reads resources from resources file.
  # @param [String] path Path of resources file
  # @return [Array<Concept::Resource>] List of resources the cluster is composed of
  #
  def self.readResources(path)
    json = JSON.parse(File.read(path))
    json.map { |res| Concept::Resource.new(res) }
  end

  # Generates a cluster object from a cluster definition file.
  # @param [String] path Path of cluster file
  # @param [Array<Concept::District>] districts List of districts the cluster can be deployed into
  # @param [Array<Concept::Resource>] resources List of resources the cluster is composed of
  # @return [Concept::Cluster] cluster object
  #
  def self.readCluster(path, districts, resources)
    clusterjson = JSON.parse(File.read(path))
    cluster = Concept::Cluster.new(clusterjson, districts, resources)

    cluster
  end

  # Generates a cluster object from cluster definition files.
  # @param [Hash] options
  # @return [Concept::Cluster] cluster object
  #
  def self.cluster(options)
    platform    = JSON.parse(File.read(options.cluster))['platform']
    credentials = Plain::readCredentials(options.credentials)
    districts   = Plain::readDistricts(options.districts, credentials, platform: platform)
    resources   = Plain::readResources(options.resources)
    cluster     = Plain::readCluster(options.cluster, districts, resources)
    cluster
  end

  # Returns a platform driver.
  # @param [String] platform Platform identifier (e.g. 'swarm', 'k8s')
  # @return [Platform::Driver] driver object, if there exist a driver for the platform
  # @return [Platform::Driver] <code>nil</code>, otherwise
  #
  def self.getPlatformDriver(platform: Platform::Swarm::PLATFORM_ID)
    return Platform::Swarm.new if platform == Platform::Swarm::PLATFORM_ID
    return Platform::K8S.new   if platform == Platform::K8S::PLATFORM_ID
    nil
  end

  # Returns a cloud service provider driver for a district.
  # @param [String] provider Provider identifier (e.g. 'GCE', 'AWS', 'Azure', OpenStack')
  # @param [Concept::District] district District object the driver is requested for
  # @return [Provider::Driver] driver object, if there exist a driver for the provider
  # @return [Provider::Driver] <code>nil</code>, otherwise
  #
  def self.getProviderDriver(provider, district, platform: Platform::Swarm::PLATFORM_ID)
    platform = getPlatformDriver(platform: platform)
    return Provider::GCE::Driver.new(district, platform) if provider.downcase == Provider::GCE::Driver::PROVIDER_ID
    return Provider::AWS::Driver.new(district, platform) if provider.downcase == Provider::AWS::Driver::PROVIDER_ID
    return Provider::Azure::Driver.new(district, platform) if provider.downcase == Provider::Azure::Driver::PROVIDER_ID
    return Provider::OpenStack::Driver.new(district, platform) if provider.downcase == Provider::OpenStack::Driver::PROVIDER_ID
    nil
  end

  # Returns a list of all registered platform identifiers (eg. 'swarm', 'k8s', and so on)
  # @return [List<Platform::Driver] List of platform identifiers
  #
  def self.platforms
    platforms = []
    platforms << Platform::Swarm::PLATFORM_ID
    platforms << Platform::K8S::PLATFORM_ID
    platforms
  end

  # Returns all provider driver objects for providers defined via command line options (--gce, --aws, --azure, --os etc.).
  # @param [Hash] options Command line options (evaluted are the options for provider drivers (--gce, --aws, --azure, --os etc.))
  # @return [List<Driver>] List of corresponding driver objects
  #
  def self.drivers(options)
    drivers = []
    drivers << Provider::GCE::Driver if options.gce
    drivers << Provider::AWS::Driver if options.aws
    drivers << Provider::Azure::Driver if options.azure
    drivers << Provider::OpenStack::Driver if options.os
    drivers
  end

  # Returns Linux Ubuntu images of cloud service providers matching provided search criteria.
  # The search is performed on a list provided via https://cloud-images.ubuntu.com/locator/releasesTable.
  # Each return entry has the following format:
  #   {
  #     'provider' => 'Provider name',
  #     'region'   => 'Region identifier',
  #     'name'     => 'Ubuntu Version name (e.g. trusty)',
  #     'version'  => 'Ubuntu Version number (e.g. 14.04)',
  #     'arch'     => 'Machine architecture (e.g. amd64)',
  #     'virt'     => 'Provider Virtualizer (e.g. kvm)',
  #     'date'     => 'Image release data (e.g. 20160716)',
  #     'image'    => 'Provider image id (or link to image, provider-specific, must be handled by driver)'
  #   }
  #
  # @param [String|RegExp] provider Provider of image (defaults to /.*/)
  # @param [String|RegExp] region Region of image (defaults to /.*/)
  # @param [String|RegExp] version Version of Ubuntu(e.g. '14.04', defaults to /.*/)
  # @param [String|RegExp] arch Host architecture for machine (e.g. 'amd64', defaults to /.*/)
  # @param [String|RegExp] virt Virtualization architecture of provider (defaults to /.*/)
  # @return [Array<Hash>] List of matching images (entries are sorted by descending date, so first entry is always the newest one).
  #
  def self.autoUbuntuImages(
      provider: /.*/,
      region:   /.*/,
      version:  /.*/,
      arch:     /.*/,
      virt:     /.*/
  )
    images = JSON.parse(File.read(Plain::options.ubuntus))
    images.select { |img| img['provider'].match(provider) }
          .select { |img| img['region'].match(region) }
          .select { |img| img['version'].match(version) }
          .select { |img| img['arch'].match(arch) }
          .select { |img| img['virt'].match(virt) }
  end

  def self.fetch_ubuntu_images(options)
    begin
      content = Net::HTTP.get(URI(options.ubuntu_images)).gsub("],\n]", "]\n]")
      Plain::log.info("Got ubuntu images from url #{options.ubuntu_images}")
      ubuntus = JSON.parse(content)
      images = ubuntus['aaData'].map do |img|
        {
            'provider' => img[0],
            'region'   => img[1],
            'name'     => img[2],
            'version'  => img[3],
            'arch'     => img[4],
            'virt'     => img[5],
            'date'     => img[6],
            'image'    => img[7]
        }
      end
      File.write(options.ubuntus, JSON.pretty_generate(images))
      Plain::log.info("Stored Ubuntu images in file #{options.ubuntus}")
    rescue Exception => ex
      error = "Could not download ubuntu images from #{options.ubuntu_images} due to '#{ex}'"
      Plain::log.error(error)
      abort(error)
    end
  end

  # Promotes CLI options as global settings throughout the plain system. This includes:
  #
  # - Command line options (CLI)
  # - Logging settings
  #
  # CLI options can be accessed via {Plain::options} throughout the system after a {Plain::promote}(options) call.
  #
  # @param [Commander::Command::Options] options CLI options passed via command line
  # @return [Logger] Logger object that must be used for logging.
  #
  def self.promote(options)

    # Set retry settings
    #
    Retryable.configure do |config|
      config.sleep = options.sleep
      config.tries = options.retries
    end

    # Set log level settings
    #
    @log = Logger.new(options.log)
    @log.level = Logger::DEBUG if options.log.downcase == 'debug'
    @log.level = Logger::INFO  if options.log.downcase == 'info'
    @log.level = Logger::WARN  if options.log.downcase == 'warn'
    @log.level = Logger::ERROR if options.log.downcase == 'error'
    @log.level = Logger::FATAL if options.log.downcase == 'fatal'

    # Promoting CLI options
    @options = options
  end

  # Logger object to use for logging.
  # @return [Logger] Logger object that must be used for logging.
  # @example Use it like that:
  #   Plain::log.error "An error log."
  #
  def self.log
    @log
  end

  # Returns all CLI options.
  # @return [Commander::Command::Options] CLI options object
  # @example Use it like that:
  #   puts("Warning message") if Plain::options.warn
  #
  def self.options
    @options
  end

  # Creates a thread to indicate progress for long running tasks.
  # @param [ProgressBar] progress progress bar object
  # @return [Thread] thread updating the progress
  #
  def self.progress_indicator(progress)
    Thread.new do
      spinner = progress.title.clone
      n = 0
      i = 1
      while(true)
        state = spinner.clone
        i = i * -1 if n + i < 0
        i = i * -1 if n + i >= spinner.length
        n = n + i
        state[n] = '*'.red
        Thread.stop if progress.finished?
        progress.title = state
        progress.refresh
        sleep(0.5)
      end
    end
  end


  def self.trace_timeline()
    timeline = File.exist?(TIMELINE_FILE) ? JSON.parse(File.read(TIMELINE_FILE)) : []
    puts "#{Plain::options.trace_as}"
    trace_as = Plain::options.trace_as
    tags = trace_as ? trace_as.split(",").map { |tag| tag.downcase.strip } : []
    puts "#{tags}"
    timeline << {
      'start'  => Time.now.tv_sec,
      'tags' => tags,
      'events' => []
    }
    File.write(TIMELINE_FILE, JSON.pretty_generate(timeline))
    timeline
  end

  def self.close_timeline(timeline)
    timeline.last['stop'] = Time.now.tv_sec
    timeline.last['duration'] = timeline.last['stop'] - timeline.last['start']
    File.write(TIMELINE_FILE, JSON.pretty_generate(timeline))
  end

  def self.add_event_to_timeline(task, timeline, &block)
    t1 = Time.now.tv_sec
    ret = block.call
    t2 = Time.now.tv_sec

    timeline.last['events'] << {
      'start' => t1,
      'end' => t2,
      'duration' => t2 - t1,
      'task' => task
    }

    File.write(TIMELINE_FILE, JSON.pretty_generate(timeline))

    ret
  end
end