require 'fog/azurerm'
require 'retryable'

module Provider

  # Microsoft Azure specific driver code.
  # @author Nane Kratzke
  #
  module Azure

    # AWS EC2 driver.
    # @author Nane Kratzke
    #
    class Driver < Provider::Driver

      PROVIDER_ID = 'azure'

      AZURE_REGION_TOPOLOGY = [
          'eastasia',
          'southeastasia',
          'centralus',
          'eastus',
          'eastus2',
          'westus',
          'westus2',
          'westcentralus',
          'northcentralus',
          'southcentralus',
          'northeurope',
          'westeurope',
          'japanwest',
          'japaneast',
          'brazilsouth',
          'australiaeast',
          'australiasoutheast',
          'southindia',
          'centralindia',
          'westindia',
          'canadacentral',
          'canadaeast',
          'uksouth',
          'ukwest',
          'koreacentral',
          'koreasouth'
      ]

      BASIC_SECURITY_RULES = [{
          name: 'allow-ssh',
          protocol: Fog::ARM::Network::Models::SecurityRuleProtocol::Tcp,
          source_port_range: '*',
          destination_port_range: '22',
          source_address_prefix: '0.0.0.0/0',
          destination_address_prefix: '0.0.0.0/0',
          access: Fog::ARM::Network::Models::SecurityRuleAccess::Allow,
          priority: '100',
          direction: Fog::ARM::Network::Models::SecurityRuleDirection::Inbound
      },{
          name: 'allow-all-out',
          protocol: Fog::ARM::Network::Models::SecurityRuleProtocol::Asterisk,
          source_port_range: '*',
          destination_port_range: '*',
          source_address_prefix: '0.0.0.0/0',
          destination_address_prefix: '0.0.0.0/0',
          access: Fog::ARM::Network::Models::SecurityRuleAccess::Allow,
          priority: '110',
          direction: Fog::ARM::Network::Models::SecurityRuleDirection::Outbound
      }]

      def initialize(district, platform)
        super(district, platform)
        @id = PROVIDER_ID
      end

      # Static method
      #
      def self.credential_template(options)
        puts("Info: You can provide your tenant ID using --az-tenant-id option") if options.az_tenant_id.nil?
        puts("Info: You can provide your subscription ID using --az-subscription-id option") if options.az_subscription_id.nil?
        puts("Info: You can provide your client ID using --az-client-id option") if options.az_client_id.nil?
        puts("Info: You can provide your secret using --az-secret option") if options.az_secret.nil?

        tenant = options.az_tenant_id.nil? ? '<SUBSCRIPTION_ID>' : options.az_tenant_id
        subscription = options.az_subscription_id.nil? ? '<SUBSCRIPTION_ID>' : options.az_tenant_id
        client = options.az_client_id.nil? ? '<CLIENT_ID>' : options.az_client_id
        secret = options.az_secret.nil? ? '<SECRET>' : options.az_secret

        {
            "type": "credential",
            "id": "#{PROVIDER_ID}_default",
            "provider": PROVIDER_ID,
            "az_tenant_id": tenant,
            "az_subscription_id": subscription,
            "az_client_id": client,
            "az_secret": secret
        }
      end

      # Static method
      # @param [] options command line options
      #
      def self.district_templates(options)
        regions = options.az_regions.nil? ? AZURE_REGION_TOPOLOGY : options.azure_regions.split(/, */)
        unknowns = regions - AZURE_REGION_TOPOLOGY
        abort("Regions '#{regions * ', '}' are unknown.") unless unknowns.empty?

        regions.map do |region|
          {
              'type' => 'district',
              'id' => "#{PROVIDER_ID}-#{region}".downcase,
              'provider' => PROVIDER_ID,
              'credential_id' => "#{PROVIDER_ID}_default",
              'azure_region' => region,
              'flavors' => [
                  {
                      'flavor' => 'small',
                      'azure_vm_size' => 'Standard_A2'
                  },
                  {
                      'flavor' => 'medium',
                      'azure_vm_size' => 'Standard_A3'
                  },
                  {
                      'flavor' => 'large',
                      'azure_vm_size' => 'Standard_A4'
                  }
              ]
          }
        end
      end

      # Checks whether command line client is installed.
      # @return [Boolean] <code>true</code> always
      #
      def cli_installed?
        true
      end

      def cli_configure
        # not necessary (CLI not used by this driver)
      end

      # Provides help text to install the Azure command line client.
      # @return [String] Help text
      #
      def cli_install_instructions
        "Azure command line tools seem to be missing on your system.\n" \
        "However, they are not necessary.\n"
      end

      # Returns resource client for Azure.
      # @private
      # @return [Fog::Resources::AzureRM] resource client
      #
      def _az_resource_client()
        Fog::Resources::AzureRM.new(
            tenant_id: self.credential.az_tenant_id,
            client_id: self.credential.az_client_id,
            client_secret: self.credential.az_secret,
            subscription_id: self.credential.az_subscription_id,
            :environment => 'AzureCloud'
        )
      end

      # Returns compute client for Azure.
      # @private
      # @return [Fog::Compute::AzureRM] resource client
      #
      def _az_compute_client()
        Fog::Compute::AzureRM.new(
            tenant_id: self.credential.az_tenant_id,
            client_id: self.credential.az_client_id,
            client_secret: self.credential.az_secret,
            subscription_id: self.credential.az_subscription_id,
            :environment => 'AzureCloud'
        )
      end

      # Returns storage client for Azure.
      # @private
      # @return [Fog::Storage] storage client
      #
      def _az_storage_client()
        Fog::Storage.new(
            provider: 'AzureRM',
            tenant_id: self.credential.az_tenant_id,
            client_id: self.credential.az_client_id,
            client_secret: self.credential.az_secret,
            subscription_id: self.credential.az_subscription_id,
            :environment => 'AzureCloud'
        )
      end

      # Returns network client for Azure.
      # @private
      # @return [Fog::Network::AzureRM] resource client
      #
      def _az_network_client()
        Fog::Network::AzureRM.new(
          tenant_id: self.credential.az_tenant_id,
          client_id: self.credential.az_client_id,
          client_secret: self.credential.az_secret,
          subscription_id: self.credential.az_subscription_id,
          :environment => 'AzureCloud'
        )
      end

      # Creates a security group for the cluster. The following detail resources are requested
      # from the Azure IaaS Infrastructure for the responsible district of the driver.
      #
      #   1. Azure resource group
      #   2. Azure security group (assigned to the resource group and attached to the default subnet of the following virtual network)
      #   3. Azure virtual network (with 10.1.0.0/16 and a default subnet of the same address space, assigned to resource group)
      #   4. Azure storage account (assigned to the resource group)
      #
      # @return [Cencept::SecurityGroup] security group object
      #
      def create_secgroup(cluster)

        rgid = "rgroup-#{cluster.platform}-#{cluster.id}-#{self.district.id}"
        g = self._az_resource_client.resource_groups.create(name: rgid, location: self.district.azure_region)
        Plain::log.info("Created azure resource group #{g.name}")

        secgroup = self._az_network_client.network_security_groups.create(
            name: "#{rgid}-secgroup",
            resource_group: g.name,
            location: self.district.azure_region,
            security_rules: BASIC_SECURITY_RULES
        )
        Plain::log.info("Created azure security group #{secgroup.name}")

        vnet = self._az_network_client.virtual_networks.create(
            name: "#{g.name}-vnet",
            location: self.district.azure_region,
            resource_group: g.name,
            address_prefixes: ['10.1.0.0/16']
        )
        Plain::log.info("Created azure virtual network #{vnet.name}")

        vnet.add_subnets [{
            name: 'default',
            address_prefix: '10.1.0.0/16',
            network_security_group_id: secgroup.id
        }]
        Plain::log.info("Created sub network for azure virtual network #{vnet.name}")

        storage_account = self._az_storage_client.storage_accounts.create(
            :name => "storage#{SecureRandom.hex(n=6)}".downcase,
            :location => self.district.azure_region,
            :resource_group => g.name,
            :account_type => 'Premium'  # Allowed values can only be Standard (HDD) or Premium (SSD)
        )
        Plain::log.info("Created storage account #{storage_account.name}")

        Concept::SecurityGroup.new({
             'id'           => g.id,
             'azure_resource_group' => g.name,
             'azure_security_group' => secgroup.name,
             'azure_virtual_net' => vnet.name,
             'azure_storage_account' => storage_account.name,
             'azure_subnet' => 'default',
             'district_id'  => self.district.id,
             'created'      => "#{Time.now}"
         })
      rescue Exception => ex
        error = "#{self.class.name}: #{ex}"
        puts(ex.backtrace * "\n")
        Plain::log.error(error)
        raise(error)
      end


      def update_secgroup(secgroup, cluster, ips)

        Plain::log.debug("Updating #{secgroup}")

        nsg = self._az_network_client.network_security_groups.get(
            secgroup.azure_resource_group,
            secgroup.azure_security_group
        )

        # Update existing rules
        updates = BASIC_SECURITY_RULES.clone
        Plain::log.debug("Generating Azure security rules for: #{ips * ", "}")
        ips.each_with_index do |ip, i|
          updates.push({
              name: "allow-#{ip}",
              protocol: Fog::ARM::Network::Models::SecurityRuleProtocol::Asterisk,
              source_port_range: '*',
              destination_port_range: '*',
              source_address_prefix: "#{ip}/32",
              destination_address_prefix: '0.0.0.0/0',
              access: Fog::ARM::Network::Models::SecurityRuleAccess::Allow,
              priority: "#{200 + i}",
              direction: Fog::ARM::Network::Models::SecurityRuleDirection::Inbound
          })
        end

        Plain::log.info("Updating #{updates.count} rules of Azure security group #{nsg.name}")
        Plain::log.debug(JSON.pretty_generate(updates))
        nsg.update_security_rules(security_rules: updates)

        Plain::log.info("Security Group #{ secgroup.id } (mapped to Azure security group #{nsg.name}) updated successfully.")

      rescue Exception => ex
        error = "#{self.class.name}: #{ex}"
        Plain::log.error(error)
        raise(error)
      end

      # Terminates a security group.
      # The following requested resources are released
      # from the Azure IaaS Infrastructure for the responsible district of the driver.
      #
      # All resources assigned to the requested resource group
      # (initially requested by {Provider::Azure::Driver.create_secgroup} method). This includes
      #
      #   1. Azure virtual network (with 10.1.0.0/16 and a default subnet of the same address space, assigned to resource group)
      #   2. Azure security group (assigned to the resource group and attached to the default subnet of the following virtual network)
      #   3. Azure storage account (assigned to the resource group)
      #   3. Azure resource group itself
      #
      # If the method returns without raising an exception the secgroup could be successfully deleted.
      #
      def terminate_secgroup(secgroup)
        resource_group = self._az_resource_client.resource_groups.get(secgroup.azure_resource_group)
        resource_group.destroy
      rescue Exception => ex
        error = "#{self.class.name}: #{ex}"
        Plain::log.error(error)
        raise(error)
      end

      # Terminates a mode and releases
      # - allocated Azure NIC resource
      # - allocated Azure public IP address resource
      #
      # If the method returns without raising an exception the node could be successfully terminated.
      #
      def terminate_node(node)
        Plain::log.info("#{self.class.name}: Terminating #{node.role} node #{node.id} in district #{self.district.id}")

        server = self._az_compute_client.servers.get(node.azure_resource_group, node.id)
        pubip = self._az_network_client.public_ips.get(node.azure_resource_group, node.azure_public_ip_name)
        nic = self._az_network_client.network_interfaces.get(node.azure_resource_group, node.azure_nic_name)

        server.destroy
        nic.destroy
        pubip.destroy

        Plain::log.info("Terminated node #{node.id} in district #{self.district.id}")
      rescue Exception => ex
        error = "#{self.class.name}: #{ex}"
        Plain::log.error(error)
        raise(error)
      end

      def request_node(flavor, role, cluster)
        Plain::log.info("#{self.class.name}: Requesting #{role} node for district #{self.district.id}")

        template = self.template(flavor)

        # Azure resource group name
        group_name = "rgroup-#{cluster.platform}-#{cluster.id}-#{self.district.id}"
        vnet_name = "#{group_name}-vnet"

        # request an Azure dynamic public IP
        pubip = self._az_network_client.public_ips.create(
            name: "ip-#{SecureRandom.uuid}",
            resource_group: group_name,
            location: self.district.azure_region,
            public_ip_allocation_method: Fog::ARM::Network::Models::IPAllocationMethod::Dynamic
        )
        Plain::log.info("Reserved public IP with id #{pubip.name}")

        # request an Azure nic
        nic = self._az_network_client.network_interfaces.create(
            name: "nic-#{SecureRandom.uuid}",
            resource_group: group_name,
            location: self.district.azure_region,
            subnet_id: "/subscriptions/#{self.credential.az_subscription_id}/resourceGroups/#{group_name}/providers/Microsoft.Network/virtualNetworks/#{vnet_name}/subnets/default",
            public_ip_address_id: pubip.id,
            ip_configuration_name: "ip-conf-#{SecureRandom.uuid}",
            private_ip_allocation_method: Fog::ARM::Network::Models::IPAllocationMethod::Dynamic
        )
        Plain::log.info("Reserved nic with id #{nic.name}")

        # request an Azure machine
        sg = cluster.secgroups.select { |sg| sg.azure_resource_group == group_name }.first
        azure_node = self._az_compute_client.servers.create(
            name: "node-#{SecureRandom.uuid}",
            location: self.district.azure_region,
            resource_group: group_name,
            vm_size: template.azure_vm_size,
            storage_account_name: sg.azure_storage_account,
            username: cluster.user,
            ssh_key_path: cluster.keyfile,
            ssh_key_data: File.read("#{cluster.keyfile}.pub"),
            disable_password_authentication: true,
            network_interface_card_ids: [nic.id],
            publisher: 'Canonical',
            offer: 'UbuntuServer',
            sku: '16.04-LTS',
            version: 'latest',
            platform: 'Linux'
        )

        # Figure out assigned public ip address
        public_ip = self._az_network_client.public_ips.get(group_name, pubip.name).ip_address

        node = Concept::Node.new({
             'id'          => azure_node.name,
             'role'        => role,
             'flavor'      => flavor,
             'public_ip'   => public_ip,
             'private_ip'  => nic.private_ip_address,
             'user'        => cluster.user,
             'sshkey'      => cluster.keyfile,
             'district_id' => self.district.id,
             'azure_resource_group' => group_name,
             'azure_zone'  => self.district.azure_region,
             'azure_nic_name' => nic.name,
             'azure_public_ip_name' => pubip.name,
             'created'     => "#{Time.now}",
         })

        Plain::log.info("#{self.class.name}: Created node #{node.id} (#{public_ip}) successfully in district #{self.district.id}.")
        node
      rescue Exception => ex
        error = "#{self.class.name}: #{ex} (Stacktrace: #{ex.backtrace * "\n"})"
        Plain::log.error(error)
        raise(error)
      end
    end
  end
end