# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'plain/version'

Gem::Specification.new do |spec|
  spec.name          = "plain"
  spec.version       = Plain::VERSION
  spec.authors       = ["Nane Kratzke"]
  spec.email         = ["nane@nkode.io"]

  spec.summary       = %q{Plain tools to deploy and operate elastic platforms.}
  spec.description   = %q{Plain tools to deploy and operate elastic platforms like K8S, Docker Swarm.}
  spec.homepage      = "http://bitbucket.org/nanekratzke/plain"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "bin"
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.10"
  spec.add_development_dependency "rake",    "~> 10.0"
  spec.add_development_dependency "yard",    "~> 0.9", ">= 0.9.0"

  spec.add_dependency 'open4ssh',         "~> 0.2.1"
  spec.add_dependency 'commander',        "~> 4.4.0"
  spec.add_dependency 'retryable',        "~> 2.0.0"
  spec.add_dependency 'colorize',         "~> 0.8.0"
  spec.add_dependency 'ruby-progressbar', "~> 1.8.0"
  spec.add_dependency 'terminal-table',   "~> 1.6.0"
  spec.add_dependency 'parallel',         "~> 1.11.0"

  spec.add_dependency 'fog-azure-rm', ">= 0.3.0", "< 0.4.0"
  spec.add_dependency 'fog-openstack', ">= 0.1.0", "< 0.2.0"
  spec.add_dependency 'aws-sdk', ">= 2.8.0", "< 3.0.0"
end
