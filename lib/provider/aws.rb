require 'aws-sdk'
require 'retryable'

module Provider

  # Amazon Web Services EC2 specific driver code.
  # @author Nane Kratzke
  #
  module AWS

    # AWS EC2 driver.
    # @author Nane Kratzke
    #
    class Driver < Provider::Driver

      PROVIDER_ID = 'aws'

      AWS_REGION_TOPOLOGY = [
          'us-east-1',
          'us-west-1',
          'us-west-2',
          'eu-west-1',
          'eu-central-1',
          'ap-southeast-1',
          'ap-southeast-2',
          'ap-northeast-1',
          'ap-northeast-2',
          'ap-south-1',
          'sa-east-1'
      ]

      BASIC_IP_PERMISSION = [
          {
             'ip_protocol' => 'tcp',
             'from_port' => 22,
             'to_port' => 22,
             'ip_ranges' => [{ 'cidr_ip' => '0.0.0.0/0' }]
          }
      ]

      def initialize(district, platform)
        super(district, platform)
        @id = PROVIDER_ID
      end

      # Static method
      #
      def self.credential_template(options)
        puts('Info: You can provide an AWS access key identifier via --aws-access-key-id option).') if options.aws_access_key_id.nil?
        puts('Info: You can provide an AWS secret access key via --aws-secret-access-key option).') if options.aws_secret_access_key.nil?

        akid = options.aws_access_key_id.nil? ? '<ACCESS KEY ID>' : options.aws_access_key_id
        secret = options.aws_secret_access_key.nil? ? '<SECRET ACCESS KEY>' : options.aws_secret_access_key

        {
            "type": "credential",
            "id": "#{PROVIDER_ID}_default",
            "provider": PROVIDER_ID,
            "aws_access_key_id": akid,
            "aws_secret_access_key": secret
        }
      end

      # Static method
      # @param [] options command line options
      #
      def self.district_templates(options)
        regions = options.aws_regions.nil? ? AWS_REGION_TOPOLOGY : options.aws_regions.split(/, */)
        unknowns = regions - AWS_REGION_TOPOLOGY
        abort("Regions '#{regions * ', '}' are unknown.") unless unknowns.empty?

        regions.map do |region|
          {
              'type' => 'district',
              'id' => "#{PROVIDER_ID}-#{region}".downcase,
              'provider' => PROVIDER_ID,
              'credential_id' => 'aws_default',
              'aws_region' => region,
              'flavors' => [
                  {
                      'flavor' => 'small',
                      'aws_instance_type' => 'm4.large'
                  },
                  {
                      'flavor' => 'medium',
                      'aws_instance_type' => 'm4.xlarge'
                  },
                  {
                      'flavor' => 'large',
                      'aws_instance_type' => 'm4.2xlarge'
                  }
              ]
          }
        end
      end

      # Checks whether AWS command line client is installed.
      # @return [Boolean] <code>true</code>, if AWS cli is installed properly.
      # @return [Boolean] <code>false</code>, if AWS cli is not installed properly.
      #
      def cli_installed?
        true
      end

      def cli_configure
        true
      end

      # Provides help text to install the AWS command line client.
      # @return [String] Help text
      #
      def cli_install_instructions
        "AWS command line tools seem to be missing on your system.\n" \
        "Please check the instructions at https://aws.amazon.com/cli\n" \
        "to install the AWS command line tools appropriately.\n"
      end

      def _ec2_client
        Aws::EC2::Resource.new(
            region: self.district.aws_region,
            access_key_id: self.credential.aws_access_key_id,
            secret_access_key: self.credential.aws_secret_access_key
        )
      end

      def create_secgroup(cluster)

        ec2 = self._ec2_client

        keyinfo = ec2.import_key_pair(
            key_name: "cluster-key-#{cluster.id}",
            public_key_material: File.read("#{cluster.keyfile}.pub")
        )

        vpc = ec2.create_vpc(cidr_block: "10.0.0.0/16")
        Plain::log.info("Created VPC #{vpc.id}")

        gw = ec2.create_internet_gateway
        gw.attach_to_vpc(vpc_id: vpc.id)
        Plain::log.info("Attached internet gateway #{gw.id} to VPC #{vpc.id}")

        sg = ec2.create_security_group(
            group_name: 'my-first-sg',
            description: 'Plain created security group',
            vpc_id: vpc.id
        )
        Plain::log.info("Created security group #{sg.id} for VPC #{vpc.id}")

        subnet = ec2.create_subnet(vpc_id: vpc.id, cidr_block: "10.0.0.0/16")
        Plain::log.info("Default subnet #{subnet.id} created for VPC #{vpc.id}")

        vpc.route_tables.each do |table|
          table.create_route(destination_cidr_block: '0.0.0.0/0', gateway_id: gw.id)
        end
        Plain::log.info("Internet routes for subnet #{subnet.id} established")

        sg.authorize_ingress(ip_permissions: BASIC_IP_PERMISSION)

        Concept::SecurityGroup.new({
             'id'           => vpc.id,
             'aws_subnet'   => subnet.id,
             'aws_key_name' => keyinfo.name,
             'aws_gateway'  => gw.id,
             'aws_security_group' => sg.id,
             'district_id'  => self.district.id,
             'created'      => "#{Time.now}"
         })
      rescue Exception => ex
        error = "#{self.class.name}: #{ex}"
        Plain::log.error(error)
        raise(error)
      end

      def update_secgroup(secgroup, cluster, ips)

        Plain::log.info("Updating #{secgroup.id}")

        ec2 = self._ec2_client

        sg = ec2.security_group(secgroup.aws_security_group)

        current_permissions = sg.ip_permissions.select { |p|
            Plain::log.debug(p.ip_protocol)
            Plain::log.debug(p.from_port)
            Plain::log.debug(p.to_port)
            (p.ip_protocol == '-1' and p.from_port == nil and p.to_port == nil)
        }

        cidrs = current_permissions.map { |perm| perm.ip_ranges.map { |r| r.cidr_ip }}.flatten
        current_ips = cidrs.map { |cidr|
          c = cidr.split("/")
          Plain::log.debug(c)
          c[0]
        }

        to_add    = ips - current_ips
        to_revoke = current_ips - ips

        Plain::log.info("Adding access for #{to_add * ', '}")
        add_permissions = [{
            'ip_protocol' => '-1',
            'from_port' => -1,
            'to_port' => -1,
            'ip_ranges' => (ips - current_ips).map { |ip| { 'cidr_ip' => "#{ip}/32" }}
        }]
        sg.authorize_ingress(ip_permissions: add_permissions) unless to_add.empty?

        Plain::log.info("Revoking access for #{to_revoke * ', '}")
        revoke_permissions = [{
            'ip_protocol' => '-1',
            'from_port' => -1,
            'to_port' => -1,
            'ip_ranges' => (current_ips - ips).map { |ip| { 'cidr_ip' => "#{ip}/32" }}
        }]
        sg.revoke_ingress(ip_permissions: revoke_permissions) unless to_revoke.empty?

        Plain::log.info("#{self.class.name}: Security Group #{ secgroup.id } updated successfully.")

      rescue Exception => ex
        error = "#{self.class.name}: #{ex}"
        Plain::log.error(error)
        raise(error)
      end

      def terminate_secgroup(secgroup)
        ec2 = self._ec2_client

        subnet = ec2.subnet(secgroup.aws_subnet)
        gw     = ec2.internet_gateway(secgroup.aws_gateway)
        sg     = ec2.security_group(secgroup.aws_security_group)
        vpc    = ec2.vpc(secgroup.id)
        key    = ec2.key_pair(secgroup.aws_key_name)

        key.delete
        Plain::log.info("Key #{key.name} deleted")

        Retryable.retryable(:sleep => 60, :tries => 3) do
          subnet.delete
          Plain::log.info("Subnet #{subnet.id} deleted")

          gw.detach_from_vpc(vpc_id: vpc.id)
          gw.delete
          Plain::log.info("Gateway #{gw.id} deleted")

          sg.delete
          Plain::log.info("Security Group #{sg.id} deleted.")

          vpc.delete
          Plain::log.info("VPC #{vpc.id} deleted")
        end

      rescue Exception => ex
        error = "#{self.class.name}: #{ex}"
        Plain::log.error(error)
        raise(error)
      end

      def terminate_node(node)
        Plain::log.info("#{self.class.name}: Terminating #{node.role} node #{node.id} in district #{self.district.id}")
        ec2 = self._ec2_client
        instance = ec2.instance(node.aws_instance_id)
        instance.terminate
        instance.wait_until_terminated if node.role == 'master'
        Plain::log.info("Terminated node #{node.id} in district #{self.district.id}")
      rescue Exception => ex
        error = "#{self.class.name}: #{ex}"
        Plain::log.error(error)
        raise(error)
      end

      def request_node(flavor, role, cluster)
        Plain::log.info("#{self.class.name}: Requesting #{role} node for district #{self.district.id}")

        ec2 = self._ec2_client

        template = self.template(flavor)

        # Figure out latest Ubuntu 16.04 image
        image = Plain::autoUbuntuImages(
            provider: 'Amazon AWS',
            version: /16.04/,
            arch: /amd64/,
            virt: /hvm-ssd/,
            region: self.district.aws_region
        ).first

        image = image.nil? ? template.aws_image : image['image'].scan(/ami-[\w\d]+/).first

        Plain::log.info("Starting instance #{template.aws_instance_type}.")

        secgroup = self.district.secgroup(cluster)

        instance = ec2.create_instances(
            image_id: image,
            min_count: 1,
            max_count: 1,
            key_name: secgroup.aws_key_name,
            instance_type: template.aws_instance_type,
            network_interfaces: [{
                 device_index: 0,
                 subnet_id: secgroup.aws_subnet,
                 groups: ["#{secgroup.aws_security_group}"],
                 delete_on_termination: true,
                 associate_public_ip_address: true
             }]
        ).first
        instance.wait_until_running
        instance = ec2.instance(instance.id)

        node = Concept::Node.new({
             'id'              => instance.id,
             'aws_instance_id' => instance.id,
             'role'            => role,
             'flavor'          => flavor,
             'public_ip'       => instance.public_ip_address,
             'private_ip'      => instance.private_ip_address,
             'user'            => cluster.user,
             'sshkey'          => cluster.keyfile,
             'district_id'     => self.district.id,
             'aws_zone'        => instance.placement.availability_zone,
             'created'         => "#{Time.now}",
         })
        Plain::log.info("#{self.class.name}: Created node #{node.id} successfully in district #{self.district.id}.")
        node
      rescue Exception => ex
        error = "#{self.class.name}: #{ex} (Stacktrace: #{ex.backtrace * "\n"})"
        Plain::log.error(error)
        raise(error)
      end
    end
  end
end