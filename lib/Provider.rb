require 'open3'
require 'open4ssh'
require 'plain'

require 'colorize'

# The {Platform} module contains modules to handle arbitrary elastic platforms like Docker (Swarm Mode), K8S, Mesos, etc.
# An elastic platform module can be included as a mixin to an provider driver.
#
# A platform mixin must provide the following methods:
#
# - install_node (to prepare a node joining an elastic platform cluster)
# - join_node (to join an elastic platform cluster as a new node)
# - drain_node (to prepare the remove of a node from an elastic platform)
# - leave (to remove the node from an elastic platform)
#
# Currently we support the following platform mixins:
#
# - {Platform::Swarm Swarm} (Docker Swarm Mode)
# - {Platform::K8S K8S} (Google Kubernetes)
#
module Platform

  # Base class to develop elastic platform drivers.
  # @author Nane Kratzke
  #
  class Driver

    # Displays a short welcome message whenever user logs into platform via login CLI command.
    #
    def welcome
      "#{self.class}: This driver should provide a helpful message here. Time for a feature request?".red
    end

    # Installs and configures necessary software on a node.
    # @note Should only be called after successful {Provider::Driver.request_node} call.
    #
    def install_node(node)
    end

    # Calling this method causes the node to join a cluster.
    # If this is the first node to join (a non existing) cluster this will init the cluster.
    # Starts installed and configured services on a machine.
    # @note Should only be called after successful {Platform::Driver.install_node} call.
    #
    def join_node(node, cluster)
    end

    # Calling this method causes the node to be drained in order to prepare the leave of the node.
    # All containers are rescheduled to other nodes.
    # Node is marked as unscheduled.
    # @note Should only be called after successful {Platform::Driver.join_node} call.
    #
    def drain_node(node, cluster)
    end

    # Calling this method causes the node to leave the cluster.
    # @note Should only be called after successful {Platform::Driver.drain_node} call.
    #
    def leave(node, cluster)
    end
  end

end

# Provides cloud service provider specific drivers.
# @author Nane Kratzke
#
module Provider

  # Base class to develop cloud service specific drivers.
  # @author Nane Kratzke
  #
  class Driver

    attr_reader :district
    attr_reader :credential
    attr_reader :platform
    attr_reader :id

    # Creates a driver object for a given district and platform.
    # @param [Concept::District] district District
    # @param [Platform::Driver] platform Platform driver
    #
    def initialize(district, platform)
      @district = district
      @platform = platform
      @credential = self.district.credential
    end

    # Validates the driver.
    # Must be overwritten by extending drivers.
    # @abstract
    #
    def validate(cluster)
      true
    end

    # @abstract
    # Checks whether the necessary command line client of a cloud service provider is installed on the system.
    #
    def cli_installed?
      false
    end

    # @abstract
    # Configure cli command line tools to work with the driver.
    #
    def cli_configure
    end

    # Provides necessary instructions to install a command line client.
    # @return [String] Install instructions
    # @abstract
    #
    def cli_install_instructions
      'Each driver should provide some helpful CLI installation instructions. Please add some.'
    end

    # Checks necessary driver preconditions and configures driver for next task.
    # @raise [Exception] If preconditions are not fulfilled.
    #
    def check_and_configure
      puts  "#{self.cli_install_instructions}"                 unless self.cli_installed?
      abort("CLI tools for for driver '#{self.id}' not found") unless self.cli_installed?
      self.cli_configure
    end

    def self.credential_template(options)
    end

    def self.district_templates(options)
    end

    # Returns the template for a specific flavor.
    # @param [String] flavor Flavor identifier
    # @return [Concept::Flavor] template for the given flavor
    #
    def template(flavor)
      self.district.template(flavor)
    end

    # @abstract
    # Requests a new machine.
    # @param [Concept::Flavor] flavor Flavor of the machine
    # @param [String] role Role of the machine
    # @param [Cluster] cluster The cluster object the node is requested for
    # @return [Concept::MachineResource] machine resource object on success
    # @return [Concept::MachineResource] <code>nil</code> on failure
    #
    def request_node(flavor, role, cluster)
    end

    # @abstract
    # Terminates a node.
    # @note Should only be called after successful {Provider::Driver.stop_services} call.
    #
    def terminate_node(node)
    end

    # @abstract
    # Creates a new security group for a district.
    # @param [Cluster] cluster The cluster object the secgroup is requested for
    # @return [Concept::SecgroupResource] secgroup resource object on success
    # @return [Concept::SecgroupResource] <code>nil</code> on failure
    # @raise  [Exception] exception with provider specific error msg on failure
    #
    def create_secgroup(cluster)
    end

    # @abstract
    # Updates security group settings to be in accordance with current cluster resources. This normally includes:
    #
    # - to ensure SSH access from 0.0.0.0/0
    # - to ensure full inner district communciation
    # - to provide access for all cluster IPs outside the own district
    # - to remove access of all IPs unknown to the cluster
    #
    # @param [Array<String>] ips List of IP addresses the security group should be open for
    # @param [Cluster] cluster The cluster object the secgroup is requested for
    #
    def update_secgroup(cluster, ips)
    end

    # @abstract
    # Deletes a security group of a district.
    # @param [Cluster] cluster The cluster object the secgroup was requested for
    # @return [Boolean] <code>true</code> on success
    #
    def terminate_secgroup(cluster)
    end

    # Installs and configures necessary software on a node via registered platform driver.
    #
    def install_node(node)
      self.platform.install_node(node)
    end

    # Calling this method causes the node to join a cluster via registered platform driver.
    #
    def join_node(node, cluster)
      self.platform.join_node(node, cluster)
    end

    # Calling this method causes the node to be drained via registered platform driver.
    #
    def drain_node(node, cluster)
      self.platform.drain_node(node, cluster)
    end

    # Calling this method causes the node to leave the cluster via registered platform driver.
    #
    def leave(node, cluster)
      self.platform.leave(node, cluster)
    end
  end
end