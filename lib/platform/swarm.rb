module Platform

  # Defines the driver capable to deploy Docker Swarm elastic platforms.
  # @author Nane Kratzke
  #
  class Swarm < Platform::Driver

    SWARM_TOKENS_FILE = 'swarm.tokens'

    PLATFORM_ID = 'swarm'

    def welcome
      swarm = "Swarm".yellow
      list_nodes = "docker node ls".yellow

      [
          "",
          "Your #{swarm} cluster is up and running.",
          "Execute one of the following commands to get started:",
          "- #{list_nodes} to list all nodes forming your cluster",
          ""
      ] * "\n"
    end

    # Installs and configures necessary software on a node.
    # @note Should only be called after successful {Provider::Driver.request_node} call.
    #
    def install_node(node)
      Plain::log.info("Installing #{node.id} started.")
      returns = Open4ssh.capture4(
          host: node.public_ip,
          user: node.user,
          key:  node.sshkey,
          cmd:  [
              'curl -fsSL https://get.docker.com/ | sh',
              'sudo usermod -aG docker $(whoami)'
          ]
      )
      raise("#{Open4ssh.console(returns)}") unless Open4ssh.success(returns)
      Plain::log.info("Installed #{node.id} successfully.")
    rescue Exception => ex
      error = "Install of node #{node.id} failed due to #{ex}. We will retry."
      Plain::log.warn(error)
      raise(error)
    end

    # Calling this method causes the node to join a cluster.
    # If this is the first node to join (a non existing) cluster this will init the cluster.
    # Starts installed and configured services on a machine.
    # @note Should only be called after successful {Provider::Driver.install_node} call.
    #
    def join_node(node, cluster)

      Plain::log.debug("#{node}")

      if cluster.masters.count == 0
        # We are the first one!
        Plain::log.info("Node #{node.id} initializes the swarm.")
        raise("Can not start a swarm with a worker node (#{node}).") unless node.role == 'master'
        exit, stdout, stderr = Open4ssh.capture3(
            host: node.public_ip,
            user: node.user,
            key:  node.sshkey,
            cmd:  "sudo docker swarm init"
        )

        if exit == 0
          # Get the tokens from output
          Plain::log.debug("#{stdout}#{stderr}")
          tokens = stdout.scan(/SWMTKN[\w\d\-]+/)
          Plain::log.debug("Tokens: #{tokens * ', '}")
          File.write(SWARM_TOKENS_FILE, JSON.pretty_generate({
                                                                 'master' => tokens[1],
                                                                 'worker' => tokens[0]
                                                             }))
        else
          raise("#{stdout}#{stderr}")
        end
        Plain::log.info("Swarm initialized successfully by node #{node.id}. Tokens written to '#{SWARM_TOKENS_FILE}'")
      else
        master = cluster.masters.sample
        Plain::log.info("Node #{node.id} is going to join the swarm as #{node.role} via master #{master.id}.")
        tokens = JSON.parse(File.read(SWARM_TOKENS_FILE))
        token = node.role == 'master' ? tokens['master'] : tokens['worker']


        exit, stdout, stderr = Open4ssh.capture3(
            host: node.public_ip,
            user: node.user,
            key:  node.sshkey,
            cmd:  "sudo docker swarm join --token #{token} #{master.public_ip}:2377"
        )

        if exit != 0
          returns = Open4ssh.capture4(
              host: node.public_ip,
              user: node.user,
              key:  node.sshkey,
              cmd:  [
                  "sudo docker swarm leave --force",
                  "sudo docker swarm join --token #{token} #{master.public_ip}:2377"
              ]
          )
          raise "Swarm join failed of node '#{node.id}' due to '#{Open4ssh.console(returns)}'" unless Open4ssh.success(returns)
        end
        Plain::log.info("Node #{node.id} joined the swarm as #{node.role} via master #{master.id} successfully.")
      end
    rescue Exception => ex
      error = "Error while joining node '#{node.id}' due to '#{ex}'"
      Plain::log.error (error)
      raise(error)
    end

    # Calling this method causes the node to be drained.
    # All containers are rescheduled to other nodes.
    # Node is marked as unscheduled.
    #
    def drain_node(node, cluster)
      # TO BE DONE
    end

    # Calling this method causes the node to leave the swarm.
    #
    def leave(node, cluster)
      cmds = []

      # We should drain the node here via a master node !
      # See https://docs.docker.com/engine/swarm/manage-nodes/#/list-nodes

      if cluster.masters.count == 1 && cluster.nodes.count > 1 && node.role == 'master'
        raise("Node #{node.id} can not leave the swarm as last master (there are still workers).")
      end

      # if cluster.nodes.count == 1 && cluster.nodes.include?(node)
      # I am the last master ...
      cmds << "docker swarm leave --force" # this is not a perfect solution
      # else
      # I am a master or a worker (but not the last)
      # cmds << "docker node demote #{node.id}" if node.role == 'master'
      # cmds << "docker swarm leave"
      #  end

      Plain::log.info("#{node.role} node #{node.id} is going to leave the cluster.")
      returns = Open4ssh.capture4(
          host: node.public_ip,
          user: node.user,
          key:  node.sshkey,
          cmd:  cmds
      )
      raise(Open4ssh.console(returns)) unless Open4ssh.success(returns)
      Plain::log.info("#{node.role} node #{node.id} left the cluster successfully.")
    rescue Exception => ex
      error = "Error while leaving node '#{node.id}' due to '#{ex}'"
      Plain::log.error(error)
      raise(error)
    end
  end
end