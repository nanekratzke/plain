require 'retryable'

module Provider

  # Google Compute Engine specific driver code.
  # @author Nane Kratzke
  #
  module GCE

    # Google Compute Engine driver.
    # @author Nane Kratzke
    #
    class Driver < Provider::Driver

      PROVIDER_ID = 'gce'
      GCE_CLI_CONFIG                 = 'plain-gce-driver-swarm'
      GCE_DISTRICT_INTERNAL_IP_RANGE = {
          'europe-west1' => '10.0.0.0/20',
          'us-west1'     => '10.10.0.0/20',
          'us-central1'  => '10.20.0.0/20',
          'us-east1'     => '10.30.0.0/20',
          'asia-east1'   => '10.40.0.0/20'
      }

      GCE_REGION_TOPOLOGY = {
          'europe-west1' => ['europe-west1-b', 'europe-west1-c', 'europe-west1-d'],
          'us-west1'     => ['us-west1-a', 'us-west1-b'],
          'us-central1'  => ['us-central1-a', 'us-central1-b', 'us-central1-c', 'us-central1-f'],
          'us-east1'     => ['us-east1-b', 'us-east1-c', 'us-east1-d'],
          'asia-east1'   => ['asia-east1-a', 'asia-east1-b', 'asia-east1-c']
      }

      def initialize(district, platform)
        super(district, platform)
        @id = PROVIDER_ID
      end

      def self.credential_template(options)
        puts('Info: You can provide a GCE service key file via the --gce-key-file option.') if options.gce_key_file.nil?
        if options.gce_key_file and not File.exist?(options.gce_key_file)
          abort("Key file #{options.gce_key_file} does not exist (--gce-key-file option).")
        end
        key_file = options.gce_key_file.nil? ? '<GCE_JSON_KEY_FILE>' : options.gce_key_file
        {
            "type": "credential",
            "id": "#{PROVIDER_ID}_default",
            "provider": PROVIDER_ID,
            "gce_key_file": key_file
        }
      end

      def self.district_templates(options)

        abort('Please provide a GCE project id via the --gce-project-id option.') if options.gce_project_id.nil?

        regions = options.gce_regions.nil? ? GCE_REGION_TOPOLOGY.keys : options.gce_regions.split(/,\w*/)
        unknowns = regions - GCE_REGION_TOPOLOGY.keys
        abort("Regions '#{regions * ', '}' are unknown (--gce-regions option).") unless unknowns.empty?

        regions.map do |region|
          {
              'type' => 'district',
              'id' => "#{PROVIDER_ID}-#{region}".downcase,
              'provider' => PROVIDER_ID,
              'credential_id' => "#{PROVIDER_ID}_default",
              'gce_project_id' => options.gce_project_id,
              'gce_region' => region,
              'gce_zones' => GCE_REGION_TOPOLOGY[region],
              'flavors' => [
                  {
                      'flavor' => 'small',
                      'gce_machine_type' => 'n1-standard-2'
                  },
                  {
                      'flavor' => 'medium',
                      'gce_machine_type' => 'n1-standard-4'
                  },
                  {
                      'flavor' => 'large',
                      'gce_machine_type' => 'n1-standard-8'
                  }
              ]
          }
        end
      end

      # Checks whether gcloud command line client is installed.
      # @return [Boolean] <code>true</code>, if gcloud cli is installed properly.
      # @return [Boolean] <code>false</code>, if gcloud cli is not installed properly.
      #
      def cli_installed?
        out, err, status = Open3.capture3('gcloud version')
        raise("gcloud version error: #{out}#{err}") if status.exitstatus != 0
        true
      rescue Exception => ex
        error = "#{self.class.name}: CLI install check failed due to '#{ex}'"
        Plain::log.error(error)
        false
      end

      def cli_configure
        json, err, status = Open3.capture3('gcloud config configurations list --format json')
        raise "#{json}#{err}" if status.exitstatus != 0
        configurations = JSON.parse(json)
        if configurations.select { |c| c['name'] == GCE_CLI_CONFIG }.empty?
          out, err, status = Open3.capture3("gcloud config configurations create #{GCE_CLI_CONFIG}")
          raise("gcloud cli configuration error: #{out}#{err}") if status.exitstatus != 0
        end
        true
      rescue Exception => ex
        error = "#{self.class.name}: CLI configure failed due to '#{ex}'"
        Plain::log.error(error)
        false
      end

      # Provides help text to install the gcloud command line client.
      # @return [String] Help text
      #
      def cli_install_instructions
        "gcloud command line tools seem to be missing on your system.\n" \
        "Please check the instructions at https://cloud.google.com/sdk/downloads\n" \
        "to install the gcloud command line tools appropriately.\n"
      end

      def create_secgroup(cluster)
        self.authenticate

        # Create a network
        networkid = "secgroup-#{cluster.id}-#{self.district.id}"
        Plain::log.info("Creating network #{networkid}")
        out, err, status = Open3.capture3("gcloud compute networks create '#{networkid}'" \
          "  --mode 'custom'" \
          "  --project '#{self.district.gce_project_id}'" \
          "  --configuration #{GCE_CLI_CONFIG}" \
          "  --format json"
        )
        raise "Create network error: #{out}#{err}" if status.exitstatus != 0

        out, err, status = Open3.capture3("gcloud compute networks subnets create '#{networkid}'" \
          "  --network '#{networkid}'" \
          "  --region '#{self.district.gce_region}'" \
          "  --range '#{GCE_DISTRICT_INTERNAL_IP_RANGE[self.district.gce_region]}'" \
          "  --project '#{self.district.gce_project_id}'" \
          "  --configuration #{GCE_CLI_CONFIG}" \
          "  --format json"
        )
        raise "Create subnet error: #{out}#{err}" if status.exitstatus != 0

        # Assign firewall rules
        out, err, status = Open3.capture3("gcloud compute firewall-rules create '#{networkid}-ssh-access'" \
          "  --project '#{self.district.gce_project_id}'" \
          "  --allow tcp:22,icmp" \
          "  --network '#{networkid}'" \
          "  --source-ranges '0.0.0.0/0'"  \
          "  --configuration #{GCE_CLI_CONFIG}" \
          "  --format json"
        )
        raise "Create ssh firewall rule error: #{out}#{err}" if status.exitstatus != 0

        srcs = cluster.nodes.map { |node| "'#{node.public_ip}/32'" } * ','
        source_ranges = srcs.empty? ? '' : "  --source-ranges #{srcs}"

        out, err, status = Open3.capture3("gcloud compute firewall-rules create '#{networkid}-cluster-access'" \
          "  --project '#{self.district.gce_project_id}'" \
          "  --allow udp,tcp,icmp" \
          "  --network '#{networkid}'" \
          "  #{source_ranges}" \
          "  --configuration #{GCE_CLI_CONFIG}" \
          "  --format json"
        )
        raise "Create cluster firewall rule error: #{out}#{err}" if status.exitstatus != 0

        Concept::SecurityGroup.new({
                                       'id'          => networkid,
                                       'district_id' => self.district.id,
                                       'created'     => "#{Time.now}",
                                       'gce_cluster_access_rules' => {
                                           'cluster' => "#{networkid}-cluster-access",
                                           'ssh'     => "#{networkid}-ssh-access"
                                       }
                                   })
      rescue Exception => ex
        error = "#{self.class.name}: #{ex}"
        Plain::log.error(error)
        raise(error)
      end

      def update_secgroup(secgroup, cluster, ips)
        unless ips.empty?
          Plain::log.info("#{self.class.name}: Updating Security Group #{ secgroup.id }.")
          self.authenticate

          rulename = secgroup.gce_cluster_access_rules['cluster']

          out, err, status = Open3.capture3("gcloud compute firewall-rules update #{rulename}" \
          "  --project '#{self.district.gce_project_id}'" \
          "  --configuration #{GCE_CLI_CONFIG}" \
          "  --allow udp,tcp,icmp" \
          "  --source-ranges #{ips.map { |ip| "#{ip}/32" } * ',' }" \
          "  --format json"
          )
          raise "Update secgroup error: #{out}#{err}" if status.exitstatus != 0
        end
        Plain::log.info("#{self.class.name}: Security Group #{ secgroup.id } updated successfully.")
      rescue Exception => ex
        error = "#{self.class.name}: #{ex}"
        Plain::log.error(error)
        raise(error)
      end

      def terminate_secgroup(secgroup)
        # Delete the network
        self.authenticate

        # Delete all firewall rules
        rules = secgroup.gce_cluster_access_rules.values * ' '
        Retryable.retryable(:tries => 3, :sleep => 60) do
          out, err, status = Open3.capture3("gcloud compute firewall-rules delete #{rules}" \
          "  --project '#{self.district.gce_project_id}'" \
          "  --configuration #{GCE_CLI_CONFIG}" \
          "  --no-user-output-enabled" \
          "  --format json"
          )
          Plain::log.warn("#{self.class.name}: Delete firewall error: #{out}#{err}") if status.exitstatus != 0
        end

        # Delete the subnetwork
        Retryable.retryable(:tries => 3, :sleep => 60) do
          out, err, status = Open3.capture3("gcloud compute networks subnets delete '#{secgroup.id}'" \
          "  --region '#{self.district.gce_region}'" \
          "  --project '#{self.district.gce_project_id}'" \
          "  --configuration #{GCE_CLI_CONFIG}" \
          "  --format json"
          )
          Plain::log.warn("#{self.class.name}: Delete subnet error: #{out}#{err}") if status.exitstatus != 0
        end

        # Delete the network
        Retryable.retryable(:tries => 3, :sleep => 60) do
          Plain::log.info("Deleting network #{secgroup.id}")
          out, err, status = Open3.capture3("gcloud compute networks delete '#{secgroup.id}'" \
          "  --project '#{self.district.gce_project_id}'" \
          "  --configuration #{GCE_CLI_CONFIG}" \
          "  --no-user-output-enabled" \
          "  --format json"
          )
          raise "Delete network error: #{out}#{err}" if status.exitstatus != 0
        end
      rescue Exception => ex
        error = "#{self.class.name}: #{ex}"
        Plain::log.error(error)
        raise(error)
      end

      def terminate_node(node)
        Plain::log.info("#{self.class.name}: Terminating #{node.role} node #{node.id} in district #{self.district.id}")

        self.authenticate

        command = "gcloud compute instances delete '#{node.id}'" \
        "  --zone '#{node.gce_zone}'" \
        "  --project '#{self.district.gce_project_id}'" \
        "  --delete-disks all" \
        "  --configuration #{GCE_CLI_CONFIG}" \
        "  --format json"

        Plain::log.debug(command)
        out, err, status = Open3.capture3(command)
        Plain::log.debug("Out: #{out}")
        Plain::log.debug("Err: #{err}")
        Plain::log.debug("Status: #{status}")

        raise("#{out}#{err}") if status.exitstatus != 0
        Plain::log.info("Terminated node #{node.id} in district #{self.district.id} successfully.")
      rescue Exception => ex
        error = "#{self.class.name}: #{ex}"
        Plain::log.error(error)
        raise(error)
      end

      # Used to authenicate the driver against a GCE service account.
      # @private
      # @raise [Exception] error, if authentication fails
      #
      def authenticate
        Plain::log.debug("Entering GCE::authenticate")
        key_file = self.credential.gce_key_file
        key_data = JSON.parse(File.read(key_file))
        Plain::log.debug("Key data: #{key_data}")

        service_account = key_data['client_email']

        authenticate = "gcloud auth activate-service-account #{service_account}" \
        "  --key-file #{key_file}" \
        "  --configuration #{GCE_CLI_CONFIG}"

        Plain::log.debug(authenticate)
        out, err, status = Open3.capture3(authenticate)
        Plain::log.debug("Out: #{out}")
        Plain::log.debug("Err: #{err}")
        Plain::log.debug("Status: #{status}")

        raise("gcloud authentication error: #{out}#{err}") if status.exitstatus != 0
      rescue Exception => ex
        error = "#{self.class.name}: #{ex}"
        Plain::log.error(error)
        raise(error)
      end

      def request_node(flavor, role, cluster)
        Plain::log.info("#{self.class.name}::request_node(): Requesting #{role} node for district #{self.district.id}")
        self.authenticate

        template = self.template(flavor)
        Plain::log.debug("#{template}")

        # Figure out latest Ubuntu 16.04 image
        image = Plain::autoUbuntuImages(
            provider: 'Google Compute Engine',
            version: /16.04/,
            arch: /amd64/,
            region: self.district.gce_region
        ).first
        image = image.nil? ? template.gce_image : "#{image['image']}"
        Plain::log.debug("Selected image: #{image}")

        zone = self.district.gce_zones.sample
        command = "gcloud compute instances create '#{cluster.id}-gce-#{SecureRandom.hex(4)}'" \
        "  --zone '#{zone}'" \
        "  --project '#{self.district.gce_project_id}'" \
        "  --machine-type '#{template.gce_machine_type}'" \
        "  --subnet '#{self.district.secgroup(cluster).id}'" \
        "  --metadata 'ssh-keys=#{cluster.user}:#{File.read("#{cluster.keyfile}.pub")}'" \
        "  --image '#{image}'" \
        "  --image-project 'ubuntu-os-cloud'" \
        "  --configuration #{GCE_CLI_CONFIG}" \
        "  --format json"

        Plain::log.debug("#{command}")
        json, err, status = Open3.capture3(command)
        Plain::log.debug("JSON: #{json}")
        Plain::log.debug("ERR: #{err}")
        Plain::log.debug("Status: #{status}")

        raise("Create instance error: #{json}#{err}") if status.exitstatus != 0

        node = JSON.parse(json)
        external_ip = node.first['networkInterfaces'].first['accessConfigs'].first['natIP']
        internal_ip = node.first['networkInterfaces'].first['networkIP']

        node = Concept::Node.new({
             'id'          => node.first['name'],
             'role'        => role,
             'flavor'      => flavor,
             'public_ip'   => external_ip,
             'private_ip'  => internal_ip,
             'user'        => cluster.user,
             'sshkey'      => cluster.keyfile,
             'district_id' => self.district.id,
             'gce_zone'    => zone,
             'created'     => "#{Time.now}",
         })
        Plain::log.info("#{self.class.name}: Created node #{node.id} successfully in district #{self.district.id}.")
        node
      rescue Exception => ex
        error = "#{self.class.name}: #{ex} (Stacktrace: #{ex.backtrace * "\n"})"
        Plain::log.error(error)
        raise(error)
      end
    end
  end

end